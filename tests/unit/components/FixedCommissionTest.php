<?php

namespace tests\unit\components;

use app\models\api\Vendor;
use app\components\FixedCommission;
use app\models\report\ReportDataRow;
use Codeception\Test\Unit;
use UnitTester;
use Yii;

class FixedCommissionTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;
    
    /**
     * @dataProvider testCanPayToVendorForCustomerProvider
     * @param float $vendorAmount
     * @param int $vendorPeriod
     * @param string $vendorEndDate
     * @param bool $expected
     */
    public function testCanPayToVendorForCustomer(float $vendorAmount, int $vendorPeriod, string $vendorEndDate, bool $expected): void
    {
        $vendor = new Vendor();
        $vendor->id = 1;
        $vendor->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => $vendorPeriod,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => $vendorAmount,
        ];
        
        $customerId = 1;
        $reportStartDate = '2021-06-01';
        
        $fixedComission = new FixedCommission($reportStartDate, []);
        $fixedComission->setVendorProperties($vendor);
        $fixedComission->setEndDate($vendor->id, $customerId, $vendorEndDate);
        
        $this->tester->assertEquals($expected, $this->tester->invokeMethod($fixedComission, 'canPayToVendorForCustomer', [$vendor->id, $customerId]));
    }

    /**
     * @return array[]
     */
    public function testCanPayToVendorForCustomerProvider(): array
    {
        return [
            'no commission with empty ' . Vendor::ENTITY . ' amount' => [0, 3, '0000-00-00', false],
            'no commission when ' . Vendor::ENTITY . ' period has finished' => [5, 3, '2021-05-01', false],
            'can pay with infinite period' => [5, 0, '0000-00-00', true],
            'can pay when period is valid' => [5, 3, '2021-08-31', true],
        ];
    }
    
    /**
     * @dataProvider testPopulateCoveredMonthsMapByCustomerProvider
     * @param string $fromDate
     * @param string $vendorEndDate
     * @param array $maxCoveredDates
     * @param array $expected
     */
    public function testPopulateCoveredMonthsMapByCustomer(string $fromDate, string $vendorEndDate, array $maxCoveredDates, array $expected): void
    {
        $vendor = new Vendor();
        $vendor->id = 1;
        $vendor->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => 3,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 5,
        ];
        
        $customerId = 1;
        $reportStartDate = '2021-06-01';
        
        $fixedComission = new FixedCommission($reportStartDate, []);
        $fixedComission->setVendorProperties($vendor);
        $fixedComission->setEndDate($vendor->id, $customerId, $vendorEndDate);

        $result = $this->tester->invokeMethod($fixedComission, 'populateCoveredMonthsMapByCustomer', [$vendor->id, $customerId, $fromDate, $maxCoveredDates]);
        $this->tester->assertEquals($expected, $result);
    }
    
    /**
     * @return array [
     *      'variant' => [
     *          (string) start date,
     *          (string) vendor end date,
     *          (array) [customer_id => (string)max covered date]
     *          (array) expected values
     *      ]
     * ]
     */
    public function testPopulateCoveredMonthsMapByCustomerProvider(): array
    {
        return [
            'no covered months' => [
                '2021-06-01',
                '2021-08-30',
                ['1' => ''],
                []
            ],
            Vendor::ENTITY . ' period finishes before max covered date' => [
                '2021-06-01', 
                '2021-07-30',
                ['1' => '2021-08-15'], 
                ['1' => [
                    '1' => ['2021-06' => 1, '2021-07' => 1]
                ]]
                
            ],
            Vendor::ENTITY . ' period finishes after max covered date' => [
                '2021-06-01', 
                '2021-08-30',
                ['1' => '2021-07-15'], 
                ['1' => [
                    '1' => ['2021-06' => 1, '2021-07' => 1]
                ]]
            ],
        ];
    }
    
    /**
     * @dataProvider testPopulateMonthsMapWithTransactionProvider
     * @param string $date
     * @param string $periodFrom
     * @param string $periodTo
     * @param array $expected
     */
    public function testPopulateMonthsMapWithTransaction(string $date, string $periodFrom, string $periodTo, array $expected): void
    {
        $vendor = new Vendor();
        $vendor->id = 1;
        $vendor->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => 3,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 5,
        ];
        
        $customerId = 1;
        $reportStartDate = '2021-06-01';
        $vendorEndDate = '2021-08-30';
        
        $transaction = new ReportDataRow();
        $transaction->date = $date;
        $transaction->period_from = $periodFrom;
        $transaction->period_to = $periodTo;
        
        $fixedComission = new FixedCommission($reportStartDate, []);
        $fixedComission->setVendorProperties($vendor);
        $fixedComission->setEndDate($vendor->id, $customerId, $vendorEndDate);
        
        // Set covered map
        $fixedComission->setMonthMap($vendor->id, $customerId, [
            '2021-06' => 1,
            '2021-07' => 1,
        ]);

        $result = $this->tester->invokeMethod($fixedComission, 'populateMonthsMapWithTransaction', [$vendor->id, $customerId, $transaction]);
        $this->tester->assertEquals($expected, $result);
    }
    
    public function testPopulateMonthsMapWithTransactionProvider(): array
    {
        return [
            'transaction period before covered date' => [
                'date' => '2021-06-01',
                'periodFrom' => '2021-06-01',
                'periodTo' => '2021-06-30',
                'expected' => ['1' => ['1' => [
                    '2021-06' => 1,
                    '2021-07' => 1,
                ]]]
            ],
            'transaction date before covered date' => [
                'date' => '2021-07-02',
                'periodFrom' => '0000-00-00',
                'periodTo' => '0000-00-00',
                'expected' => ['1' => ['1' => [
                    '2021-06' => 1,
                    '2021-07' => 1,
                ]]]
            ],
            'transaction period after covered date' => [
                'date' => '2021-06-01',
                'periodFrom' => '2021-06-01',
                'periodTo' => '2021-09-15',
                'expected' => ['1' => ['1' => [
                    '2021-06' => 1,
                    '2021-07' => 1,
                    '2021-08' => 0,
                    '2021-09' => 0,
                ]]]
            ],
            'transaction date after covered date' => [
                'date' => '2021-08-30',
                'periodFrom' => '0000-00-00',
                'periodTo' => '0000-00-00',
                'expected' => ['1' => ['1' => [
                    '2021-06' => 1,
                    '2021-07' => 1,
                    '2021-08' => 0,
                ]]]
            ],
        ];
    }
    
    /**
     * @return void
     */
    public function testPopulateMap(): void
    {
        $fixedComission = new FixedCommission('0000-00-00', []);
        
        $variants = $this->getVariantsForPopulateMap();
        
        // Test init map - just months
        $monthMap = $this->tester->invokeMethod($fixedComission, 'populateMap', [[], $variants['covered']['start_date'], $variants['covered']['max_covered_date'], 1]);
        $this->tester->assertEquals($variants['covered']['expected'], $monthMap);

        // Test covered map - few months are covered
        $transactionsMap = $this->tester->invokeMethod($fixedComission, 'populateMap', [$monthMap, $variants['transactions']['start_date'], $variants['transactions']['max_covered_date'], 0]);
        $this->tester->assertEquals($variants['transactions']['expected'], $transactionsMap);
    }
    
    /**
     * @return array[]
     */
    public function getVariantsForPopulateMap(): array
    {
        return [
            'covered' => [
                'start_date' => '2021-06-01',
                'max_covered_date' => '2021-07-22',
                'expected' => [
                    '2021-06' => 0,
                    '2021-07' => 0,                  
                ]
            ],
            'transactions' => [
                'start_date' => '2021-06-01',
                'max_covered_date' => '2021-09-30',
                'expected' => [
                    '2021-06' => 0,
                    '2021-07' => 0,
                    '2021-08' => 1,
                    '2021-09' => 1,                    
                ]
            ],
        ];
    }
    
    /**
     * @dataProvider testCalculateFixedAmountByCustomerProvider
     * @param int $vendorPeriod
     * @param string $vendorEndDate
     * @param array $monthMap
     * @param array|bool $expected
     * @return void
     */
    public function testCalculateFixedAmountByCustomer(int $vendorPeriod, string $vendorEndDate, array $monthMap, $expected): void
    {
        $vendor = new Vendor();
        $vendor->id = 1;
        $vendor->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => $vendorPeriod,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 5,
        ];
        
        $customerId = 1;
        $reportStartDate = '2021-06-01';
        
        $fixedComission = new FixedCommission($reportStartDate, []);
        $fixedComission->setVendorProperties($vendor);
        $fixedComission->setEndDate($vendor->id, $customerId, $vendorEndDate);
        $fixedComission->setMonthMap($vendor->id, $customerId, $monthMap);

        $result = $this->tester->invokeMethod($fixedComission, 'calculateFixedAmountByCustomer', [$vendor->id, $customerId]);
        $this->tester->assertEquals($expected, $result);
    }
    
    /**
     * @return array [
     *      (int) vendor period
     *      (string) vendor period end date
     *      (array) map of covered and new months
     *      (array) expected
     * ]
     */
    public function testCalculateFixedAmountByCustomerProvider()
    {
        $formatter = Yii::$app->getFormatter();
        
        return [
            Vendor::ENTITY . ' period has finished' => [
                2, 
                '2022-07-30', 
                [
                    '2021-06' => 1,
                    '2021-07' => 1,
                ],
                false
            ],
            Vendor::ENTITY . ' period has not finished' => [
                3, 
                '2022-09-30', 
                [
                    '2021-06' => 1,
                    '2021-07' => 1,
                    '2021-08' => 0,
                ],
                [
                    'amount' => 5,
                    'monthsForComission' => 1,
                    'description' => $formatter->asDate('2021-08-01', 'MMM Y'),
                ]
            ],
            'infinitive ' . Vendor::ENTITY . ' period' => [
                0, 
                '2022-09-30', 
                [
                    '2021-06' => 1,
                    '2021-07' => 1,
                    '2021-08' => 0,
                    '2021-09' => 0,
                    '2021-10' => 0,
                    '2021-11' => 0,
                    '2021-12' => 0,
                    '2022-01' => 0,
                    '2022-02' => 0,
                    '2022-03' => 0,
                    '2022-04' => 0,
                    '2022-05' => 0,
                    '2022-06' => 0,
                    '2022-07' => 0,
                    '2022-08' => 0,
                    '2022-09' => 0,
                ],
                [
                    'amount' => 70,
                    'monthsForComission' => 14,
                    'description' => $formatter->asDate('2021-08-01', 'MMM Y') . ' - ' . $formatter->asDate('2022-09-01', 'MMM Y'),
                ]
            ],
        ];
    }
    
    /**
     * @return void
     */
    public function testGetVendorPayPeriodMaxDate(): void
    {
        $fixedComission = new FixedCommission('0000-00-00', []);
        
        $variants = $this->getVariantsForGetVendorPayPeriodMaxDate();
        foreach($variants as $variant) {
            $result = $this->tester->invokeMethod($fixedComission, 'getVendorPayPeriodMaxDate', [$variant['start_date'], $variant[Vendor::ENTITY . '_period']]);
            $this->tester->assertEquals($variant['expected'], $result);
        }
    }
    
    /**
     * @return array[]
     */
    public function getVariantsForGetVendorPayPeriodMaxDate(): array
    {
        return [
            [
                'start_date' => '2021-06-01',
                Vendor::ENTITY . '_period' => 0,
                'expected' => ''
            ],
            [
                'start_date' => '2021-06-01',
                Vendor::ENTITY . '_period' => 1,
                'expected' => '2021-06-30'
            ],
            [
                'start_date' => '2021-01-01',
                Vendor::ENTITY . '_period' => 2,
                'expected' => '2021-02-28'
            ],
            [
                'start_date' => '2021-01-01',
                Vendor::ENTITY . '_period' => 5,
                'expected' => '2021-05-31'
            ],
            [
                'start_date' => '2021-06-01',
                Vendor::ENTITY . '_period' => 15,
                'expected' => '2022-08-31'
            ],
        ];
    }
    
    /**
     * @return void
     */
    public function testGetMonthsBetweenTwoDates(): void
    {
        $fixedComission = new FixedCommission('0000-00-00', []);
        
        $variants = $this->getVariantsForGetMonthsBetweenTwoDates();
        foreach($variants as $variant) {
            $result = $this->tester->invokeMethod($fixedComission, 'getMonthsBetweenTwoDates', [$variant['start_date'], $variant['end_date']]);
            $this->tester->assertEquals($variant['expected'], $result);   
        }
    }

    /**
     * @return array[]
     */
    public function getVariantsForGetMonthsBetweenTwoDates(): array
    {
        return [
            [
                'start_date' => '2021-02-04', 
                'end_date' => '2021-02-06',
                'expected' => ['2021-02']
            ],
            [
                'start_date' => '2021-02-28', 
                'end_date' => '2021-03-01',
                'expected' => ['2021-02', '2021-03']
            ],
            [
                'start_date' => '2021-01-01', 
                'end_date' => '2021-03-01',
                'expected' => ['2021-01', '2021-02', '2021-03']
            ],
            [
                'start_date' => '2021-01-05', 
                'end_date' => '2021-06-30',
                'expected' => ['2021-01', '2021-02', '2021-03', '2021-04', '2021-05', '2021-06']
            ],
            [
                'start_date' => '2021-03-03', 
                'end_date' => '2022-05-05',
                'expected' => ['2021-03', '2021-04', '2021-05', '2021-06', '2021-07', '2021-08', '2021-09', '2021-10', '2021-11', '2021-12', '2022-01', '2022-02', '2022-03', '2022-04', '2022-05']
            ],
        ];
    }
}
