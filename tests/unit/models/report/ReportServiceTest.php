<?php

namespace tests\unit\models\report;

use app\helpers\DateHelper;
use app\helpers\ReportDataHelper;
use app\helpers\TransactionCategoriesHelper;
use app\models\api\Customer;
use app\models\api\Vendor;
use app\models\db\Invoices;
use app\models\report\ReportDataRepository;
use app\models\report\ReportDataRow;
use app\models\report\ReportService;
use Codeception\Stub;
use Codeception\Test\Unit;
use Exception;
use splynx\base\ApiResponseException;
use UnitTester;
use Yii;
use yii\db\Exception as DbException;
use yii\helpers\ArrayHelper;

/**
 * Class ReportServiceTest
 */
class ReportServiceTest extends Unit
{
    /**
     * @var UnitTester
     */
    protected $tester;

    public static $oldDateFormat;
    public const TEST_DATE_FORMAT = 'd.m.y';

    public function _before()
    {
        static::$oldDateFormat = DateHelper::getSplynxDateFormat();
        ReportDataHelper::setCustomSplynxDateFormat(static::TEST_DATE_FORMAT);
    }

    public function _after()
    {
        ReportDataHelper::setCustomSplynxDateFormat(static::$oldDateFormat);
    }

    /**
     * @dataProvider isTransactionValidForCustomerProvider
     * @param ReportDataRow $transaction
     * @param string $startPeriod
     * @param int $vendorPeriods
     * @param bool $expected
     */
    public function testIsTransactionValidForCustomer(ReportDataRow $transaction, string $startPeriod, int $vendorPeriods, bool $expected)
    {
        $categories = [TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND];
        $service = new ReportService($categories);
        $this->assertEquals($expected, $this->tester->invokeMethod($service, 'isTransactionValidForCustomer', [$transaction, $startPeriod, $vendorPeriods]));
    }

    /**
     * @return array[]
     */
    public function isTransactionValidForCustomerProvider(): array
    {
        $transactions = [
            'invalid_date_less' => [
                'date' => '2020-01-01',
                'period_from' => '0000-00-00',
                'period_to' => '0000-00-00',
            ],
            'invalid_period_to_less' => [
                'date' => '2021-01-01',
                'period_from' => '0000-00-00',
                'period_to' => '2020-12-31',
            ],
            'invalid_finite_period_date_more' => [
                'date' => '2021-02-01',
                'period_from' => '0000-00-00',
                'period_to' => '0000-00-00',
            ],
            'invalid_finite_period_from_more' => [
                'date' => '2021-01-01',
                'period_from' => '2021-02-01',
                'period_to' => '2021-02-28',
            ],
            'normal_date' => [
                'date' => '2021-01-01',
                'period_from' => '0000-00-00',
                'period_to' => '0000-00-00',
            ],
            'normal_period' => [
                'date' => '2021-02-01',
                'period_from' => '2021-01-01',
                'period_to' => '2021-01-31',
            ],
        ];

        $populatedTransactions = [];
        foreach ($transactions as $transaction) {
            $model = new ReportDataRow();
            $populatedTransactions[] = $this->populateObject($model, $transaction);
        }

        return [
            'finite periods past date' => [$populatedTransactions[0], '2021-01-01', 1, false],
            'finite periods past period_to' => [$populatedTransactions[1], '2021-01-01', 1, false],
            'infinite periods past date' => [$populatedTransactions[0], '2021-01-01', 0, false],
            'infinite periods past period_to' => [$populatedTransactions[1], '2021-01-01', 0, false],
            'finite periods end date less than date' => [$populatedTransactions[2], '2021-01-01', 1, false],
            'finite periods end date less than period_from' => [$populatedTransactions[3], '2021-01-01', 1, false],
            'infinite periods on case "end date less than date"' => [$populatedTransactions[2], '2021-01-01', 0, true],
            'infinite periods on case "end date less than period_from"' => [$populatedTransactions[3], '2021-01-01', 0, true],
            'finite periods normal date' => [$populatedTransactions[4], '2021-01-01', 1, true],
            'finite periods normal period' => [$populatedTransactions[4], '2021-01-01', 1, true],
            'infinite periods on case "normal date"' => [$populatedTransactions[4], '2021-01-01', 0, true],
            'infinite periods on case "normal period"' => [$populatedTransactions[4], '2021-01-01', 0, true],
        ];
    }

    /**
     * @dataProvider calculatePeriodsLeftProvider
     * @param string $startPeriod
     * @param string $targetPeriod
     * @param int $periods
     * @param int $expected
     */
    public function testCalculatePeriodsLeft(string $startPeriod, string $targetPeriod, int $periods, int $expected)
    {
        $categories = [TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND];
        $service = new ReportService($categories);
        self::assertEquals($expected, $this->tester->invokeMethod($service, 'calculatePeriodsLeft', [$startPeriod, $targetPeriod, $periods]));
    }

    /**
     * @return array[]
     */
    public function calculatePeriodsLeftProvider(): array
    {
        return [
            'infinite periods past date' => ['2021-01-01', '2020-12-31', 0, 0],
            'infinite periods same month' => ['2021-01-01', '2021-01-01', 0, 0],
            'infinite periods next year start month' => ['2021-01-01', '2022-01-01', 0, 0],
            'finite 1 period past date' => ['2021-01-01', '2020-12-31', 1, 0],
            'finite 1 period same month' => ['2021-01-01', '2021-01-31', 1, 1],
            'finite 1 period next month' => ['2021-01-01', '2021-02-01', 1, 0],
            'finite 2 periods next month' => ['2021-01-01', '2021-02-28', 2, 1],
            'finite 2 periods +2 month' => ['2021-01-01', '2021-03-01', 2, 0],
        ];
    }

    /**
     * @dataProvider calculateValidEndPeriodProvider
     * @param string $startPeriod
     * @param string $targetPeriod
     * @param int $periods
     * @param string $expected
     */
    public function testCalculateValidEndPeriod(string $startPeriod, string $targetPeriod, int $periods, string $expected)
    {
        $categories = [TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND];
        $service = new ReportService($categories);
        self::assertEquals($expected, $this->tester->invokeMethod($service, 'calculateValidEndPeriod', [$startPeriod, $targetPeriod, $periods]));
    }

    /**
     * @return array[]
     */
    public function calculateValidEndPeriodProvider(): array
    {
        return [
            'infinite periods past date' => ['2021-01-01', '2020-12-31', 0, '2021-01-31'],
            'infinite periods same month' => ['2021-01-01', '2021-01-01', 0, '2021-01-01'],
            'infinite periods next year start month' => ['2021-01-01', '2022-01-01', 0, '2022-01-01'],
            'finite 1 period past date' => ['2021-01-01', '2020-12-31', 1, '2021-01-31'],
            'finite 1 period same month' => ['2021-01-01', '2021-01-31', 1, '2021-01-31'],
            'finite 1 period next month' => ['2021-01-01', '2021-02-01', 1, '2021-01-31'],
            'finite 2 periods next month' => ['2021-01-01', '2021-02-28', 2, '2021-02-28'],
            'finite 2 periods +2 month' => ['2021-01-01', '2021-03-01', 2, '2021-02-28'],
        ];
    }

    /**
     * @dataProvider calculateVendorCommissionProvider
     * @param ReportDataRow $transaction
     * @param float $commissionPercent
     * @param int $vendorPeriods
     * @param string $startPeriod
     * @param float $expected
     */
    public function testCalculateVendorCommission(ReportDataRow $transaction, float $commissionPercent, int $vendorPeriods, string $startPeriod, float $expected)
    {
        $categories = [TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND];
        $service = new ReportService($categories);
        self::assertEquals($expected, $this->tester->invokeMethod($service, 'calculateVendorCommission', [$transaction, $commissionPercent, $vendorPeriods, $startPeriod]));
    }

    /**
     * @return array[]
     */
    public function calculateVendorCommissionProvider(): array
    {
        $populate = function ($object, $data) {
            foreach ($data as $attribute => $value) {
                $object->$attribute = $value;
            }

            return $object;
        };

        $transactions = [
            [
                'date' => '2021-01-01',
                'period_from' => '0000-00-00',
                'period_to' => '0000-00-00',
                'total' => 100,
                'type' => ReportDataRow::TYPE_DEBIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '0000-00-00',
                'period_to' => '0000-00-00',
                'total' => 100,
                'type' => ReportDataRow::TYPE_CREDIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-01-01',
                'period_to' => '2021-01-31',
                'total' => 100,
                'type' => ReportDataRow::TYPE_DEBIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-01-01',
                'period_to' => '2021-01-31',
                'total' => 100,
                'type' => ReportDataRow::TYPE_CREDIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-01-01',
                'period_to' => '2021-12-31',
                'total' => 1200,
                'type' => ReportDataRow::TYPE_DEBIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-01-01',
                'period_to' => '2021-12-31',
                'total' => 1200,
                'type' => ReportDataRow::TYPE_CREDIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-03-25',
                'period_to' => '2021-04-30',
                'total' => 370,
                'type' => ReportDataRow::TYPE_DEBIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-03-25',
                'period_to' => '2021-04-30',
                'total' => 370,
                'type' => ReportDataRow::TYPE_CREDIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-01-01',
                'period_to' => '2022-12-31',
                'total' => 2400,
                'type' => ReportDataRow::TYPE_DEBIT,
            ],
            [
                'date' => '2021-01-01',
                'period_from' => '2021-01-01',
                'period_to' => '2022-12-31',
                'total' => 2400,
                'type' => ReportDataRow::TYPE_CREDIT,
            ],
        ];

        $populatedTransactions = [];
        foreach ($transactions as $transaction) {
            $model = new ReportDataRow();
            $populate($model, $transaction);
            $populatedTransactions[] = $model;
        }

        $vendors = [
            [
                'percent' => 5,
                'periods' => 3,
            ],
            [
                'percent' => 10,
                'periods' => 0,
            ],
            [
                'percent' => 10,
                'periods' => 18,
            ],
        ];
        $startDate = '2021-01-01';

        return [
            'only date debit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[0], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, 5],
            'only date credit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[1], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, -5],
            '1 out of 1 month debit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[2], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, 5],
            '1 out of 1 month credit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[3], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, -5],
            '7 days range out of 1 month debit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[2], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, 5],
            '7 days range out of 1 month credit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[3], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, -5],
            '7 commissioned days out of 1 month debit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[6], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, 3.5],
            '7 commissioned days out of 1 month credit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[7], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, -3.5],
            '3 out of 12 month debit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[4], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, 14.79],
            '3 out of 12 month credit / 3 periods ' . Vendor::ENTITY => [$populatedTransactions[5], $vendors[0]['percent'], $vendors[0]['periods'], $startDate, -14.79],
            '12 out of 12 month debit / infinite periods ' . Vendor::ENTITY => [$populatedTransactions[4], $vendors[1]['percent'], $vendors[1]['periods'], $startDate, 120],
            '12 out of 12 month credit / infinite periods ' . Vendor::ENTITY => [$populatedTransactions[5], $vendors[1]['percent'], $vendors[1]['periods'], $startDate, -120],
            '24 out of 12 month debit / infinite periods ' . Vendor::ENTITY => [$populatedTransactions[4], $vendors[1]['percent'], $vendors[1]['periods'], $startDate, 120],
            '24 out of 12 month credit / infinite periods ' . Vendor::ENTITY => [$populatedTransactions[5], $vendors[1]['percent'], $vendors[1]['periods'], $startDate, -120],
            '18 out of 24 month debit / infinite periods ' . Vendor::ENTITY => [$populatedTransactions[8], $vendors[2]['percent'], $vendors[2]['periods'], $startDate, 179.51],
            '18 out of 24 month credit / infinite periods ' . Vendor::ENTITY => [$populatedTransactions[9], $vendors[2]['percent'], $vendors[2]['periods'], $startDate, -179.51],
        ];
    }

    /**
     * @throws Exception
     * @return void
     */
    public function testGenerateVendorsReport(): void
    {
        $data = $this->generateMockDataForVendorsReport();
        $expectedResults = $this->getVendorsReportsResponses();
        // January
        $invoices = ArrayHelper::index($this->getInvoices('vendors', 'january'), 'id');
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => $data['findStartDatesForReport'],
                'findData' => $this->getTransactions('vendors', 'january'),
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findVendorsForReport' => $data['findVendorsForReport'],
//                'generateVendorLink' => '---',
                'findUnpaidInvoicesByIds' => Stub::consecutive([], [$invoices[2218], $invoices[2220]], [$invoices[2223]]),
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $this->assertEquals($expectedResults['january'], $reportServiceMock->generateVendorsReport('2021-01-01', '2021-01-31'), 'Mismatch in report for January');


        //february
        $invoices = ArrayHelper::index($this->getInvoices('vendors', 'february'), 'id');
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => $data['findStartDatesForReport'],
                'findData' => $this->getTransactions('vendors', 'february'),
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findVendorsForReport' => $data['findVendorsForReport'],
//                'generateVendorLink' => '---',
                'findUnpaidInvoicesByIds' => Stub::consecutive([$invoices[2213], $invoices[2215]], [$invoices[2219], $invoices[2221]], [$invoices[2225]]),
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $this->assertEquals($expectedResults['february'], $reportServiceMock->generateVendorsReport('2021-02-01', '2021-02-28'), 'Mismatch in report for February');

        //march
        $invoices = ArrayHelper::index($this->getInvoices('vendors', 'march'), 'id');
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => $data['findStartDatesForReport'],
                'findData' => $this->getTransactions('vendors', 'march'),
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findVendorsForReport' => $data['findVendorsForReport'],
//                'generateVendorLink' => '---',
                'findUnpaidInvoicesByIds' => Stub::consecutive([$invoices[2216]], [$invoices[2222]]),
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $this->assertEquals($expectedResults['march'], $reportServiceMock->generateVendorsReport('2021-03-01', '2021-03-31'), 'Mismatch in report for March');

        // january_to_march
        $invoices = ArrayHelper::index($this->getInvoices('vendors', 'january_to_march'), 'id');
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => $data['findStartDatesForReport'],
                'findData' => $this->getTransactions('vendors', 'january_to_march'),
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findVendorsForReport' => $data['findVendorsForReport'],
//                'generateVendorLink' => '---',
                'findUnpaidInvoicesByIds' => Stub::consecutive([$invoices[2213], $invoices[2215], $invoices[2216]], [$invoices[2218], $invoices[2219], $invoices[2220], $invoices[2221], $invoices[2222]], [$invoices[2223], $invoices[2225]]),
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $this->assertEquals($expectedResults['january_to_march'], $reportServiceMock->generateVendorsReport('2021-01-01', '2021-03-31'), 'Mismatch in report from January to March');
    }

    /**
     * @return array
     */
    protected function generateMockDataForVendorsReport(): array
    {
        $vendor1 = new Vendor();
        $vendor1->id = 1;
        $vendor1->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => 2,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 0,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
        ];
        $vendor2 = new Vendor();
        $vendor2->id = 2;
        $vendor2->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '2'),
            Vendor::PAY_PERIOD => 0,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 0,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
        ];
        $vendor3 = new Vendor();
        $vendor3->id = 3;
        $vendor3->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '3'),
            Vendor::PAY_PERIOD => 1,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 0,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
        ];

        return [
            'findVendorsForReport' => [1 => $vendor1, 2 => $vendor2, 3 => $vendor3],
            'findStartDatesForReport' => [
                1998 => '2021-01-01',
                1999 => '2021-02-01',
                2000 => '2021-01-01',
                2001 => '2021-01-01',
                2002 => '2021-01-01',
                2003 => '2021-02-01',
            ],
        ];
    }

    /**
     * @return array
     */
    protected function getVendorsReportsResponses(): array
    {
        return [
            'january' => [
                1 => [
                    'vendor_id' => 1,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '1'), '2021-01-01', '2021-01-31', 1),
                    'active_customers_count' => 1,
                    'total_sum' => '300.00',
                    'commission_total' => '30.00',
                    'unpaid_invoices_total' => '0.00',
                ],
                2 => [
                    'vendor_id' => 2,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '2'), '2021-01-01', '2021-01-31', 2),
                    'active_customers_count' => 2,
                    'total_sum' => '200.00',
                    'commission_total' => '20.00',
                    'unpaid_invoices_total' => '200.00',
                ],
                3 => [
                    'vendor_id' => 3,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '3'), '2021-01-01', '2021-01-31', 3),
                    'active_customers_count' => 1,
                    'total_sum' => '1200.00',
                    'commission_total' => '10.19',
                    'unpaid_invoices_total' => '1200.00',
                ],
            ],
            'february' => [
                1 => [
                    'vendor_id' => 1,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '1'), '2021-02-01', '2021-02-28', 1),
                    'active_customers_count' => 2,
                    'total_sum' => '350.00',
                    'commission_total' => '35.00',
                    'unpaid_invoices_total' => '350.00',
                ],
                2 => [
                    'vendor_id' => 2,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '2'), '2021-02-01', '2021-02-28', 2),
                    'active_customers_count' => 2,
                    'total_sum' => '200.00',
                    'commission_total' => '20.00',
                    'unpaid_invoices_total' => '200.00',
                ],
                3 => [
                    'vendor_id' => 3,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '3'), '2021-02-01', '2021-02-28', 3),
                    'active_customers_count' => 1,
                    'total_sum' => '100.00',
                    'commission_total' => '10.00',
                    'unpaid_invoices_total' => '100.00',
                ],
            ],
            'march' => [
                1 => [
                    'vendor_id' => 1,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '1'), '2021-03-01', '2021-03-31', 1),
                    'active_customers_count' => 1,
                    'total_sum' => '100.00',
                    'commission_total' => '10.00',
                    'unpaid_invoices_total' => '100.00',
                ],
                2 => [
                    'vendor_id' => 2,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '2'), '2021-03-01', '2021-03-31', 2),
                    'active_customers_count' => 1,
                    'total_sum' => '100.00',
                    'commission_total' => '10.00',
                    'unpaid_invoices_total' => '100.00',
                ],
            ],
            'january_to_march' => [
                1 => [
                    'vendor_id' => 1,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '1'), '2021-01-01', '2021-03-31', 1),
                    'active_customers_count' => 2,
                    'total_sum' => '700.00',
                    'commission_total' => '70.00',
                    'unpaid_invoices_total' => '450.00',
                ],
                2 => [
                    'vendor_id' => 2,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '2'), '2021-01-01', '2021-03-31', 2),
                    'active_customers_count' => 2,
                    'total_sum' => '500.00',
                    'commission_total' => '50.00',
                    'unpaid_invoices_total' => '500.00',
                ],
                3 => [
                    'vendor_id' => 3,
                    'name' => ReportDataHelper::generateVendorLink(ucfirst(Vendor::ENTITY . '3'), '2021-01-01', '2021-03-31', 3),
                    'active_customers_count' => 2,
                    'total_sum' => '1300.00',
                    'commission_total' => '20.19',
                    'unpaid_invoices_total' => '1300.00',
                ],
            ],
        ];
    }

    /**
     * @param string $report Valid: 'vendors', 'vendor', 'customer'
     * @param string $period Valid: 'january', 'february', 'march', 'january_to_march'
     * @return array
     */
    protected function getInvoices(string $report, string $period): array
    {
        $basePath = codecept_data_dir();
        $path = "$basePath/$report-report/report_invoices_$period.csv";

        return $this->getDataFromCsv($path, Invoices::class);
    }

    /**
     * @param string $path
     * @param string $class
     * @return array
     */
    protected function getDataFromCsv(string $path, string $class): array
    {
        $row = 1;
        $columns = [];
        $models = [];
        $handle = fopen($path, 'rb');
        while (($data = fgetcsv($handle)) !== false) {
            if ($row === 1) {
                $row++;
                $columns = $data;
                continue;
            }
            $row++;
            $model = Yii::createObject($class);

            foreach ($data as $key => $value) {
                if (isset($columns[$key])) {
                    $model->{$columns[$key]} = $value;
                }
            }
            $models[] = $model;
        }
        fclose($handle);

        return $models;
    }

    /**
     * @param string $path
     * @return array
     * 6138 => [
     * 'billing_transactions_id' => 6138,
     * 'category' => 7,
     * 'description' => 'Test 100',
     * 'date' => '2021-01-20',
     * 'period_from' => '',
     * 'period_to' => '',
     * 'invoice_id' => '2210',
     * 'total' => 100.0,
     * 'commission' => 10.0,
     * ],
     */
    protected function getExpectedResultsFromCsv(string $path): array
    {
        if (file_exists($path)) {
            $row = 1;
            $columns = [];
            $results = [];
            $handle = fopen($path, 'rb');
            while (($data = fgetcsv($handle)) !== false) {
                if ($row === 1) {
                    $row++;
                    $columns = $data;
                    continue;
                }
                $row++;

                $currentKey = 0;
                foreach ($data as $key => $value) {
                    if ($columns === null) {
                        continue;
                    }

                    if ($key == 0) {
                        $currentKey = $value;
                        $results[$currentKey] = array();
                    }
                    if (isset($columns[$key])) {
                        $results[$currentKey][$columns[$key]] = $value;
                    }
                }
            }
            fclose($handle);

            return $results;
        }
        return [];
    }

    protected function getTransactionsVariant(string $report, string $variantName): array
    {
        $basePath = codecept_data_dir();
        $path = "$basePath$report-report/$variantName.csv";

        return $this->getDataFromCsv($path, ReportDataRow::class);
    }

    /**
     * @param string $report Valid: 'vendors', 'vendor', 'customer'
     * @param string $period Valid: 'january', 'february', 'march', 'january_to_march'
     * @return array
     */
    protected function getTransactions(string $report, string $period): array
    {
        $basePath = codecept_data_dir();
        $path = "$basePath/$report-report/report_rows_$period.csv";

        return $this->getDataFromCsv($path, ReportDataRow::class);
    }

    /**
     * @throws ApiResponseException
     * @throws Exception
     * @return void
     */
    public function testGenerateVendorReport(): void
    {
        $vendor1 = new Vendor();
        $vendor1->id = 1;
        $vendor1->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => 3,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 0,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
        ];

        $expectedResults = $this->getVendorReportResponses();
        // February has records
        $invoices = ArrayHelper::index($this->getInvoices('vendor', 'february'), 'id');
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => [1998 => '2021-01-01', 1999 => '2021-02-01'],
                'findData' => $this->getTransactions('vendor', 'february'),
                'findCustomersNames' => [],
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findVendorById' => $vendor1,
//                'generateCustomerLink' => '---',
                'findUnpaidInvoicesByIds' => Stub::consecutive([$invoices[2213]], [$invoices[2215]]),
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $this->assertEquals($expectedResults['february'], $reportServiceMock->generateVendorReport('2021-02-01', '2021-02-28', 1), 'Mismatch in report result data');

        // No records
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => [],
                'findData' => [],
                'findCustomersNames' => [],
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findVendorById' => $vendor1,
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $this->assertEquals($expectedResults['no_data'], $reportServiceMock->generateVendorReport('2021-02-01', '2021-02-28', 1), 'Mismatch in report no data');
    }

    /**
     * @return array
     */
    protected function getVendorReportResponses(): array
    {
        return [
            'february' => [
                1998 => [
                    'customer_id' => 1998,
                    'customer_name' => ReportDataHelper::generateCustomerLink('---', '2021-02-01', '2021-02-28', 1998),
                    'transactions_count' => 2,
                    'total_sum' => 350,
                    'commission_total' => 35,
                    'unpaid_invoices_ids' => [2213],
                    'unpaid_invoices_total' => 300,
                ],
                1999 => [
                    'customer_id' => 1999,
                    'customer_name' => ReportDataHelper::generateCustomerLink('---', '2021-02-01', '2021-02-28', 1999),
                    'transactions_count' => 2,
                    'total_sum' => 50,
                    'commission_total' => 5,
                    'unpaid_invoices_ids' => [2215],
                    'unpaid_invoices_total' => 50,
                ],
            ],
            'no_data' => [],
        ];
    }

    /**
     * @throws Exception
     * @throws ApiResponseException
     * @return void
     */
    public function testGenerateCustomerReport(): void
    {
        $vendor1 = new Vendor();
        $vendor1->id = 1;
        $vendor1->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => 2,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 0,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
        ];

        $customer = new Customer();
        $customer->additional_attributes[Vendor::ADD_ON_ENTITY] = 1;
        $expectedResults = $this->getCustomerReportResponses();
        // February has records
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => [1998 => '2021-01-01'],
                'findData' => $this->getTransactions('customer', 'january'),
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findCustomerById' => $customer,
                'findVendorById' => $vendor1,
//                'generateCustomerLink' => '---',
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $executingResults = $reportServiceMock->generateCustomerReport('2021-01-01', '2021-01-31', 1998);
        \Yii::info($executingResults, 'test');
        $this->assertEquals($expectedResults['january'], $executingResults, 'Mismatch in report result data');

        // No records
        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => [],
                'findData' => [],
                'findMaxCoveredDate' => [],
            ]
        );
        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findCustomerById' => null,
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);
        $this->assertEquals($expectedResults['no_data'], $reportServiceMock->generateCustomerReport('2021-01-01', '2021-01-31', 1998), 'Mismatch in report no data');
    }

    /**
     * @return array
     */
    protected function getCustomerReportResponses(): array
    {
        return [
            'january' => [
                6138 => [
                    'billing_transactions_id' => 6138,
                    'category' => 7,
                    'description' => 'Test 100',
                    'date' => '2021-01-20',
                    'period_from' => '',
                    'period_to' => '',
                    'invoice_id' => '2210',
                    'total' => 100.0,
                    'commission' => 10.0,
                ],
                6140 => [
                    'billing_transactions_id' => 6140,
                    'category' => 1,
                    'description' => 'Test 100 bundle',
                    'date' => '2021-01-01',
                    'period_from' => '2021-01-01',
                    'period_to' => '2021-01-31',
                    'invoice_id' => '',
                    'total' => 300.0,
                    'commission' => 30.0,
                ],
                6141 => [
                    'billing_transactions_id' => '6141',
                    'category' => '2',
                    'description' => 'Discount for Test 100 bundle',
                    'date' => '2021-01-01',
                    'period_from' => '2021-01-01',
                    'period_to' => '2021-01-31',
                    'invoice_id' => '',
                    'total' => '-150.00',
                    'commission' => '0.00',
                ],
                6142 => [
                    'billing_transactions_id' => '6142',
                    'category' => '1',
                    'description' => 'Test 50 bundle',
                    'date' => '2020-12-01',
                    'period_from' => '2021-01-01',
                    'period_to' => '2021-01-31',
                    'invoice_id' => '',
                    'total' => '50.00',
                    'commission' => '5.00',
                ],
            ],
            'no_data' => [],
        ];
    }

    /**
     * @dataProvider fixedAmountForConsolidatedReportProvider
     * @param string $startDate
     * @param string $endDate
     * @param string $recordSet
     * @param string $expected
     * @return void
     * @throws DbException
     */
    public function testFixedAmountForConsolidatedReport(string $startDate, string $endDate, string $recordSet, string $expected): void
    {
        $data = $this->getMockDataForFixedAmountInConsolidatedReport();

        $expectedResults = $this->getReportExpectedResults('vendors', $expected);

        /** @var ReportDataRepository $reportDataRepositoryMock */
        $reportDataRepositoryMock = Stub::construct(
            ReportDataRepository::class,
            [],
            [
                'findStartDates' => $data['findStartDatesForReport'],
                'findData' => $this->getTransactionsVariant('vendors', $recordSet),
                'findMaxCoveredDate' => [],
            ]
        );

        /** @var ReportService $reportServiceMock */
        $reportServiceMock = Stub::construct(
            ReportService::class,
            [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
            [
                'findVendorsForReport' => $data['findVendorsForReport'],
//                'generateVendorLink' => '---',
                'findUnpaidInvoicesByIds' => [],
            ]
        );
        $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);

        $executingResults = $reportServiceMock->generateVendorsReport($startDate, $endDate);

        $this->assertEquals($expectedResults, $executingResults, 'Mismatch in report result data');
    }

    /**
     * @return array
     */
    protected function getMockDataForFixedAmountInConsolidatedReport(): array
    {
        $vendor1 = new Vendor();
        $vendor1->id = 1;
        $vendor1->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PAY_PERIOD => 2,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 5,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
        ];
        $vendor2 = new Vendor();
        $vendor2->id = 2;
        $vendor2->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '2'),
            Vendor::PAY_PERIOD => 0,
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 5,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
        ];

        return [
            'findVendorsForReport' => [1 => $vendor1, 2 => $vendor2],
            'findStartDatesForReport' => [
                1999 => '2021-10-01',
                1998 => '2021-10-01',
                1997 => '2021-10-01',
                1996 => '2021-11-01',
            ],
        ];
    }

    /**
     * @return array<string, string[]>
     */
    public static function fixedAmountForConsolidatedReportProvider(): array
    {
        return [
            'sep: no transactions' => [
                'startDate' => '2021-09-01',
                'endDate' => '2021-09-30',
                'recordSet' => 'record_set_empty',
                'expected' => 'expected_empty'
            ],
            'oct: ' . Vendor::ENTITY . '1(2p) has 3 customers (1+2months), ' . Vendor::ENTITY . '2(0p) has 1 customer (3months)' => [
                'startDate' => '2021-10-01',
                'endDate' => '2021-10-31',
                'recordSet' => 'record_set_oct',
                'expected' => 'expected_oct'
            ],
            'oct-dec: ' . Vendor::ENTITY . '1(2p) has 2 customers (1+2+2months), ' . Vendor::ENTITY . '2(0p) has 1 customer (3months)' => [
                'startDate' => '2021-10-01',
                'endDate' => '2021-12-31',
                'recordSet' => 'record_set_oct-dec',
                'expected' => 'expected_oct-dec'
            ],
        ];
    }

    /**
     * @dataProvider fixedAmountForVendorReportProvider
     * @param int $vendorPeriod
     * @param array $reportPeriods
     * @return void
     * @throws ApiResponseException
     * @throws Exception
     */
    public function testFixedAmountForVendorReport(int $vendorPeriod, array $reportPeriods): void
    {
        $vendor1 = new Vendor();
        $vendor1->id = 1;
        $vendor1->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 5,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
            Vendor::PAY_PERIOD => $vendorPeriod,
        ];

        foreach ($reportPeriods as $reportPeriod) {
            $expectedResults = $this->getReportExpectedResults('vendor', $reportPeriod['result']);

            /** @var ReportDataRepository $reportDataRepositoryMock */
            $reportDataRepositoryMock = Stub::construct(
                ReportDataRepository::class,
                [],
                [
                    'findStartDates' => [1998 => '2021-06-01', 1999 => '2021-07-01'],
                    'findData' => $this->getTransactionsVariant('vendor', $reportPeriod['record_set']),
                    'findCustomersNames' => [],
                    'findMaxCoveredDate' => [],
                ]
            );
            /** @var ReportService $reportServiceMock */
            $reportServiceMock = Stub::construct(
                ReportService::class,
                [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
                [
                    'findVendorById' => $vendor1,
//                    'generateCustomerLink' => '---',
                    'findUnpaidInvoicesByIds' => [], // Stub::consecutive([$invoices[2213]], [$invoices[2215]]),
                ]
            );
            $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);

            foreach ($expectedResults as $customerId => $fields) {
                foreach ($fields as $key => $value) {
                    if ($key == 'transactions_count') {
                        $expectedResults[$customerId][$key] = (int)$value;
                    }
                    if ($key == 'unpaid_invoices_ids') {
                        $expectedResults[$customerId][$key] = array();
                    }
                }
            }
            $executingResults = $reportServiceMock->generateVendorReport($reportPeriod['start'], $reportPeriod['end'], $vendor1->id);
            $this->assertEquals($expectedResults, $executingResults, 'Mismatch in report result data');
        }
    }

    /**
     * @return array[variant][
     *      (int) vendor period
     *      (array) [
     *          period name => [
     *              'start' => (string) start date of period
     *              'end' => (string) end date of period
     *              'record_set' => (string) name of record set file
     *              'result' => (string) name of file with expected result
     *          ]
     *      ]
     * ]
     */
    public static function fixedAmountForVendorReportProvider(): array
    {
        return [
            '0 commissions before started period' => [
                3,
                'reportPeriods' => [
                    'may' => [
                        'start' => '2021-05-01',
                        'end' => '2021-05-31',
                        'record_set' => 'record_set_empty',
                        'result' => 'expected_empty'
                    ],
                ]
            ],
            '2+1 months commission with period3' => [
                3,
                'reportPeriods' => [
                    'jun-jul' => [
                        'start' => '2021-06-01',
                        'end' => '2021-07-30',
                        'record_set' => 'record_set_jun-jul', // 1998 incl. jun-jul transactions; 1999 incl.jul tr.only
                        'result' => 'expected_2+1m3p'
                    ],
                ]
            ],
            '2+2 months commission with period3' => [
                3,
                'reportPeriods' => [
                    'jun-aug' => [
                        'start' => '2021-06-01',
                        'end' => '2021-08-31',
                        'record_set' => 'record_set_jun-jul_jul-aug', // 1998 incl. jun-jul transactions; 1999 incl.jul-aug tr.
                        'result' => 'expected_2+2m3p'
                    ],
                ]
            ],
            '3 months commission with period3 and 4 covered months' => [
                3,
                'reportPeriods' => [
                    'jun-sep' => [
                        'start' => '2021-06-01',
                        'end' => '2021-09-30',
                        'record_set' => 'record_set_jun-sep', // 1998 incl. jun-sep transactions; 1999 incl.jul-aug tr.
                        'result' => 'expected_3+1m3p4c'
                    ],
                ]
            ],
            '12 month commission with period0' => [
                0,
                'reportPeriods' => [
                    'jun-may' => [
                        'start' => '2021-06-01',
                        'end' => '2022-05-30',
                        'record_set' => 'record_set_year', // includes tr. started in june that covered 12 months
                        'result' => 'expected_12m0p'
                    ],
                ]
            ],
        ];
    }

    /**
     * @dataProvider fixedAmountForCustomerReportProvider
     * @param int $vendorPeriod
     * @param array $reportPeriods
     * @return void
     * @throws ApiResponseException
     * @throws DbException
     */
    public function testFixedAmountForCustomerReport(int $vendorPeriod, array $reportPeriods): void
    {
        $vendor1 = new Vendor();
        $vendor1->id = 1;
        $vendor1->additional_attributes = [
            Vendor::NAME => ucfirst(Vendor::ENTITY . '1'),
            Vendor::PERCENT => 10,
            Vendor::FIXED_AMOUNT => 5,
            Vendor::NEW_CUSTOMER_PERCENT => 0,
            Vendor::PAY_PERIOD => $vendorPeriod,
        ];

        $customer = new Customer();
        $customer->id = 1998;
        $customer->additional_attributes[Vendor::ADD_ON_ENTITY] = 1;

        $customerStartDate = '2021-06-01';

        foreach ($reportPeriods as $reportPeriod) {
            $expectedResults = $this->getReportExpectedResults('customer', $reportPeriod['result']);

            /** @var ReportDataRepository $reportDataRepositoryMock */
            $reportDataRepositoryMock = Stub::construct(
                ReportDataRepository::class,
                [],
                [
                    'findStartDates' => [$customer->id => $customerStartDate],
                    'findData' => $this->getTransactionsVariant('customer', $reportPeriod['record_set']),
                    'findMaxCoveredDate' => [],
                ]
            );
            /** @var ReportService $reportServiceMock */
            $reportServiceMock = Stub::construct(
                ReportService::class,
                [[TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND, TransactionCategoriesHelper::SERVICE]],
                [
                    'findCustomerById' => $customer,
                    'findVendorById' => $vendor1,
//                    'generateCustomerLink' => '---',
                ]
            );
            $reportServiceMock->setReportDataRepository($reportDataRepositoryMock);

            $executingResults = $reportServiceMock->generateCustomerReport($reportPeriod['start'], $reportPeriod['end'], $customer->id);
            $this->assertEquals($expectedResults, $executingResults, 'Mismatch in report result data');
        }
    }

    /**
     * @return array[variant][
     *      (int) vendor period
     *      (array) [
     *          period name => [
     *              'start' => (string) start date of period
     *              'end' => (string) end date of period
     *              'record_set' => (string) name of record set file
     *              'result' => (string) name of file with expected result
     *          ]
     *      ]
     * ]
     */
    public static function fixedAmountForCustomerReportProvider(): array
    {
        return [
            '0 comissions before started period' => [
                3,
                [
                    'may' => [
                        'start' => '2021-05-01',
                        'end' => '2021-05-31',
                        'record_set' => 'record_set_empty',
                        'result' => 'expected_empty'
                    ],
                ]
            ],
            '2 months commission with period3 and 2 covered months' => [
                3,
                [
                    'jun' => [
                        'start' => '2021-06-01',
                        'end' => '2021-06-31',
                        'record_set' => 'record_set_jun-jul', // Includes tr. started in june that covered 2 months
                        'result' => 'expected_2m3p2c'
                    ],
                    'jun-jul' => [
                        'start' => '2021-06-01',
                        'end' => '2021-07-30',
                        'record_set' => 'record_set_jun-jul',
                        'result' => 'expected_2m3p2c'
                    ],
                    'jun-aug' => [
                        'start' => '2021-06-01',
                        'end' => '2021-08-31',
                        'record_set' => 'record_set_jun-jul',
                        'result' => 'expected_2m3p2c'
                    ],
                ]
            ],
            '3 months commission with period3 and 4 covered months' => [
                3,
                [
                    'jun-jul' => [
                        'start' => '2021-06-01',
                        'end' => '2021-07-30',
                        'record_set' => 'record_set_jun-sep', // Includes tr. started in june that covered 4 months
                        'result' => 'expected_3m3p4c'
                    ],
                    'jun-sep' => [
                        'start' => '2021-06-01',
                        'end' => '2021-09-30',
                        'record_set' => 'record_set_jun-sep',
                        'result' => 'expected_3m3p4c'
                    ],
                ]
            ],
            '3 months commission with period3 and 2+2 covered months' => [
                3,
                [
                    'jun-sep' => [
                        'start' => '2021-06-01',
                        'end' => '2021-09-30',
                        'record_set' => 'record_set_jun-jul_aug-sep', // includes tr. started in july that covered 2 months
                        'result' => 'expected_3m3p22c'
                    ],
                ]
            ],
            '1 month commission with period3 and 2+2 covered months' => [
                3,
                [
                    'aug-sep' => [
                        'start' => '2021-08-01',
                        'end' => '2021-09-30',
                        'record_set' => 'record_set_aug-sep', // includes tr. started in july that covered 2 months
                        'result' => 'expected_1m3p22c'
                    ],
                ]
            ],
            '12 month commission with period0 and 12 covered months' => [
                0,
                [
                    'jun-may' => [
                        'start' => '2021-06-01',
                        'end' => '2022-05-30',
                        'record_set' => 'record_set_year', // includes tr. started in june that covered 12 months
                        'result' => 'expected_12m0p12c'
                    ],
                ]
            ],
        ];
    }

    /**
     * @param string $reportType
     * @param string $variantName
     * @return array
     */
    protected function getReportExpectedResults(string $reportType, string $variantName): array
    {
        $basePath = codecept_data_dir();
        $path = "$basePath/$reportType-report/$variantName.csv";

        return $this->getExpectedResultsFromCsv($path);
    }

    /**
     * @param object $object
     * @param array $data Attribute_name/value pairs
     * @return object
     */
    protected function populateObject(object $object, array $data): object
    {
        foreach ($data as $attribute => $value) {
            $object->$attribute = $value;
        }

        return $object;
    }

    /**
     * @return void
     * @throws DbException
     */
    public function testGenerateVendorsReportForSeveralMonths(): void
    {
        $object = new class () extends ReportDataRepository {
            /**
             * @var array<int, string>
             */
            public $arraySql = [];

            /**
             * @inheridoc
             */
            public function runCommandAndGetData(string $sql): array
            {
                $this->arraySql[] = $sql;
                return [];
            }
        };

        $this->assertEquals([], $object->findStartDates([1], [1], []));

        $object->findStartDates([1], [1], [1, 2, 3]);
        $this->assertEquals([
            "        SELECT customer_id, MIN(date) as date
        FROM billing_transactions
        RIGHT JOIN customers_values
            ON billing_transactions.customer_id = customers_values.id
            AND customers_values.name = 'splynx_addon_agents_agent'
            AND customers_values.value <> ''
        WHERE deleted = '0'
        AND category IN ('1','2','3') AND customers_values.value IN ('1') AND customer_id IN ('1') GROUP BY customer_id",
            "        SELECT customer_id, MIN(period_from) as period_from
        FROM billing_transactions
        RIGHT JOIN customers_values
            ON billing_transactions.customer_id = customers_values.id
            AND customers_values.name = 'splynx_addon_agents_agent'
            AND customers_values.value <> ''
        WHERE deleted = '0' AND period_from <> '0000-00-00' AND category IN ('1','2','3') AND customers_values.value IN ('1') AND customer_id IN ('1') GROUP BY customer_id",
        ], $object->arraySql);
    }
}
