<?php

namespace Mock\models\db;

use app\models\db\Invoices;

/**
 * Class InvoicesMock
 * @package Mock\models\db
 */
class InvoicesMock extends Invoices
{
    /**
     * Prevents db calls for unit tests
     * @return string[]
     */
    public function attributes(): array
    {
        return [
            'id',
            'customer_id',
            'number',
            'date_created',
            'real_create_datetime',
            'date_updated',
            'date_payment',
            'date_till',
            'total',
            'status',
            'payment_id',
            'is_sent',
            'payd_from_deposit',
            'use_transactions',
            'note',
            'memo',
            'added_by',
            'added_by_id',
            'deleted',
        ];
    }
}
