<?php
namespace Helper;

// here you can define custom actions
// all public methods declared in helper class will be available in $I
use ReflectionException;

/**
 * Class Unit
 * @package Helper
 */
class Unit extends \Codeception\Module
{

    /**
     * @param object $object
     * @param string $methodName
     * @param array $parameters
     * @return mixed
     * @throws ReflectionException
     */
    public function invokeMethod(object $object, string $methodName, array $parameters = [])
    {
        $reflection = new \ReflectionClass(get_class($object));
        $method = $reflection->getMethod($methodName);
        $method->setAccessible(true);

        return $method->invokeArgs($object, $parameters);
    }
}
