<?php
define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);

require(__DIR__ . '/../../splynx-addon-base-2/vendor/autoload.php');
require(__DIR__ . '/../../splynx-addon-base-2/vendor/yiisoft/yii2/Yii.php');

// Load add-on vendor
require(__DIR__ . '/../vendor/autoload.php');

$baseDir = __DIR__ . '/../';
$configPath = $baseDir . '/config/test.php';

$application = new splynx\base\ConsoleApplication($baseDir, $configPath);
