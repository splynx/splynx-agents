<?php

use splynx\helpers\ConfigHelper;

$dbConfig = ConfigHelper::getSplynxConfig('mysql')['db'];
$dbConfig['password'] = exec('/var/www/splynx/system/script/security decrypt --hash="' . $dbConfig['password'] . '"');

$config = [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=' . $dbConfig['host'] . ';dbname=' . $dbConfig['database'],
    'username' => $dbConfig['username'],
    'password' => $dbConfig['password'],
    'charset' => $dbConfig['charset'],
    'tablePrefix' => 'SAA_',
];

if (YII_ENV_PROD) {
    $config['enableLogging'] = false;
    $config['enableProfiling'] = false;
}

return $config;
