<?php

use app\models\db\Invoices;
use Mock\models\db\InvoicesMock;

return function ($params, $baseDir) {
    return yii\helpers\ArrayHelper::merge(require 'common.php', [
        'components' => [
            'formatter' => [
                'dateFormat' => 'php:Y-m-d',
            ],
        ],
        'container' => [
            'definitions' => [
                Invoices::class => InvoicesMock::class,
            ],
        ],
    ]);
};
