<?php

use splynx\helpers\DateHelper;

return [
    'bootstrap' => [
        'log',
        'app\base\Bootstrap',
    ],
    'components' => [
        'db' => require 'db.php',
        'formatter' => [
            'thousandSeparator' => '',
            'numberFormatterOptions' => [
                NumberFormatter::MIN_FRACTION_DIGITS => 2,
                NumberFormatter::MAX_FRACTION_DIGITS => 2,
            ],
            'dateFormat' => 'php:' . DateHelper::getSplynxDateFormat(),
        ],
    ],
];
