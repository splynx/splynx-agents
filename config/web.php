<?php

use splynx\models\administration\BaseAdministrator;

return function ($params, $baseDir) {
    return yii\helpers\ArrayHelper::merge(require 'common.php', [
        'components' => [
            'request' => [
                'baseUrl' => '/agents',
                'enableCookieValidation' => false,
            ],
            'user' => [
                'identityClass' => BaseAdministrator::class,
                'idParam' => 'splynx_admin_id',
                'loginUrl' => '/admin/login/?return=%2Fagents/',
                'enableAutoLogin' => false,
            ],
            'assetManager' => [
                'appendTimestamp' => true,
            ],
            'view' => [
                'renderers' => [
                    'twig' => [
                        'globals' => [
                            'Tabs' => ['class' => '\yii\bootstrap\Tabs'],
                        ],
                    ],
                ],
            ],
        ],
    ]);
};
