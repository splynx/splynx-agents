<?php

namespace app\models\cron;

use app\components\NewCustomerCommission;
use Yii;
use yii\base\Model;

/**
 * Class CronDataRepository
 * Contains methods to process cron logic
 * @package app\models\cron
 */
class CronDataRepository extends Model
{
    /**
     * get all customers who have NewCustomerCommission::ADF_AMOUNT > 0
     * but whose internet service is already not active
     * and NewCustomerCommission::ADF_CANCELLATION_DATE is not filled
     * @return array
     */
    public function findCustomersWithCancelledServices()
    {
        $params = [
            ':adf_amount' => NewCustomerCommission::ADF_AMOUNT,
            ':adf_cancellation_date' => NewCustomerCommission::ADF_CANCELLATION_DATE,
        ];

        $sql = "
        SELECT A.id, B.id as service_id, B.status, B.start_date 
        FROM customers A
        LEFT JOIN services_internet B ON (A.id = B.customer_id)
        LEFT JOIN customers_values C ON (A.id = C.id) 
        WHERE A.id IN (
                SELECT id FROM customers_values 
                WHERE name = :adf_amount 
                    AND value != ''
                    AND cast(value as decimal(9,2)) > 0
            )
            AND (C.name = :adf_cancellation_date) AND (C.value = '')
            AND B.status <> 'active' AND B.start_date <= CURDATE()";

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql, $params);

        $dataSet = $command->queryAll();

        return $dataSet;
    }
}
