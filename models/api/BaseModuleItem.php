<?php

namespace app\models\api;

use splynx\base\ApiResponseException;
use splynx\v2\base\BaseActiveApi;
use splynx\v2\helpers\ApiHelper;
use yii\base\InvalidConfigException;

/**
 * Class BaseModuleItem
 * @package app\models\api
 *
 * @property-read mixed $moduleName
 */
abstract class BaseModuleItem extends BaseActiveApi
{
    public $id;
    public static $apiUrl = 'admin/config/module';

    /**
     * @inheritdoc
     * @param null $id
     * @param array $conditions
     * @return string
     */
    protected function getApiUrl($id = null, $conditions = []): string
    {
        $result = self::$apiUrl . '/' . $this->getModuleName();

        // Set id
        if ($id !== null) {
            $result .= '--' . $id;
        }

        // Set condition
        if ($conditions !== []) {
            $result .= '?' . http_build_query($conditions);
        }

        return $result;
    }

    /**
     * Return module name to access
     * @return string
     */
    abstract protected function getModuleName(): string;

    /**
     * Delete service. Method is overwritten because in services we use custom URLs
     * @return bool
     * @throws ApiResponseException
     * @throws InvalidConfigException
     */
    public function delete(): bool
    {
        $result = ApiHelper::getInstance()->delete($this->getApiUrl($this->id), null);

        return $result['result'];
    }
}
