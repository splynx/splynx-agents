<?php

namespace app\models\api;

use splynx\base\ApiResponseException;
use splynx\base\BaseBatchApiResult;
use splynx\v2\models\customer\BaseCustomer;

/**
 * Class Customer
 * @package app\models\api
 */
class Customer extends BaseCustomer
{
    use EntityTrait;

    /**
     * @param array $ids
     * @return Customer[]
     */
    public static function findByIds(array $ids): array
    {
        return (new self())->findAll(['id' => ['IN', $ids]]);
    }

    /**
     * @param integer $id
     * @return BaseBatchApiResult|Customer[]
     */
    public static function findByVendorId(int $id): BaseBatchApiResult
    {
        return (new self())->each([], [Vendor::ADD_ON_ENTITY => $id]);
    }

    /**
     * @param string $name
     * @return Customer[]
     */
    public static function findByName(string $name): array
    {
        return (new self())->findAll(['name' => ['LIKE', $name]]);
    }

    /**
     * @param int $id
     * @return Vendor|null
     * @throws ApiResponseException
     */
    public static function findByIdStatic(int $id): ?Customer
    {
        return (new self())->findById($id);
    }
}
