<?php

namespace app\models\api;

use splynx\base\BaseBatchApiResult;
use splynx\v2\models\finance\BaseTransactions;

/**
 * Class Transactions
 * @package app\models\splynx
 */
class Transactions extends BaseTransactions
{
    /**
     * @param $id
     * @return Transactions|null
     */
    public static function findByIdStatic($id): ?Transactions
    {
        return (new static())->findOne(['id' => $id]);
    }

    /**
     * Searches by transaction dates
     * @param integer $customerId
     * @param string $from Y-m-d date
     * @param string $to Y-m-d date
     * @param array $categories
     * @return BaseBatchApiResult|Transactions[]
     */
    public static function getCustomerTransactionsForPeriodByDate(int $customerId, string $from, string $to, array $categories): BaseBatchApiResult
    {
        return (new static())->each(['customer_id' => $customerId, 'date' => ['BETWEEN', $from, $to], 'category' => ['IN', $categories]]);
    }

    /**
     * Searches by transaction period dates
     * @param integer $customerId
     * @param string $from Y-m-d date
     * @param string $to Y-m-d date
     * @param array $categories
     * @return BaseBatchApiResult|Transactions[]
     */
    public static function getCustomerTransactionsForPeriodByPeriod(int $customerId, string $from, string $to, array $categories): BaseBatchApiResult
    {
        return (new static())->each(['customer_id' => $customerId, 'period_from' => ['>=', $from], 'period_to' => ['<=', $to], 'category' => ['IN', $categories]]);
    }
}
