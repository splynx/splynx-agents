<?php

namespace app\models\api;

use splynx\base\ApiResponseException;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class Vendor
 * @package app\models\api
 */
class Vendor extends BaseModuleItem
{
    public $additional_attributes = [];

    public const NAME = self::MODULE_NAME . '_name'; // Required
    public const PAY_PERIOD = self::MODULE_NAME . '_pay_period';
    public const PERCENT = self::MODULE_NAME . '_percent';
    public const FIXED_AMOUNT = self::MODULE_NAME . '_fixed_amount';
    public const NEW_CUSTOMER_PERCENT = self::MODULE_NAME . '_start_percent';
    public const EMAIL = self::MODULE_NAME . '_email';
    public const MODULE_NAME = 'splynx_mod_agents';
    public const ADD_ON_ENTITY = 'splynx_addon_agents_agent';

    public const PAY_PERIOD_INFINITE = 0;
    public const ENTITY = 'agent';
    public const ENTITIES = self::ENTITY . 's';

    /**
     * @param array $ids
     * @return Vendor[]
     */
    public static function findByIds(array $ids): array
    {
        return (new self())->findAll(['id' => ['IN', $ids]]);
    }

    /**
     * @inheritdoc
     * @return string
     */
    protected function getModuleName(): string
    {
        return self::MODULE_NAME;
    }

    /**
     * @param string $name
     * @return Vendor[]
     */
    public static function findByName(string $name): array
    {
        return (new self())->findAll([], [self::NAME => ['LIKE', $name]]);
    }

    /**
     * @return array
     */
    public static function getItemsForSelect(): array
    {
        $result = [];
        foreach (self::getListAll() as $value) {
            $result[$value->id] = $value->additional_attributes[self::NAME];
        }

        return $result;
    }

    /**
     * @param int $id
     * @return Vendor|null
     * @throws ApiResponseException
     */
    public static function findByIdStatic(int $id): ?Vendor
    {
        return (new self())->findById($id);
    }

    /**
     * @param array $ids
     * @return Vendor[]
     */
    public static function findForReport(array $ids = []): array
    {
        return empty($ids) ?
            ArrayHelper::index(static::getListAll(), 'id') :
            ArrayHelper::index(static::findByIds($ids), 'id');
    }
    public function isAllowedNewCustomerCommission()
    {
        return (!empty((float)$this->additional_attributes[self::NEW_CUSTOMER_PERCENT]));
    }

    /**
     * @return string
     */
    public static function getEntitiesName()
    {
        return Yii::t('app', 'Agents');
    }
}
