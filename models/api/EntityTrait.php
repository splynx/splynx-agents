<?php

namespace app\models\api;

use Yii;

trait EntityTrait
{
    /**
     * @param int $id
     * @return mixed
     */
    public function findById($id)
    {
        try {
            return parent::findById($id);
        } catch (\Exception $e) {
            Yii::error(print_r($e, true));
        }
        return null;
    }
}
