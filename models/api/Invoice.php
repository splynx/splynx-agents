<?php

namespace app\models\api;

use splynx\v2\models\finance\BaseInvoice;

/**
 * Class Invoice
 * @package app\models\splynx
 *
 * @property-read Transactions[]|null $transactions
 */
class Invoice extends BaseInvoice
{
    /**
     * @return Transactions[]|null
     */
    public function getTransactions(): ?array
    {
        return (new Transactions())->findAll(['invoiced_by_id' => $this->id]);
    }

    /**
     * @param array $ids
     * @return Invoice[]|null
     */
    public static function findUnpaidByIds(array $ids): ?array
    {
        return (new static())->findAll(['id' => ['IN', $ids], 'status' => ['!=', BaseInvoice::STATUS_PAID]]);
    }
}
