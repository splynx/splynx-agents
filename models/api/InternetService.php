<?php

namespace app\models\api;

use app\models\api\EntityTrait;
use splynx\v2\models\services\BaseInternetService;

/**
 * Class InternetService
 * @package app\models\splynx
 */
class InternetService extends BaseInternetService
{
    use EntityTrait;
}
