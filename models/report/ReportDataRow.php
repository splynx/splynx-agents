<?php

namespace app\models\report;

/**
 * Class ReportDataRow
 * PDO will populate this class with string values
 * @package app\models\report
 * @property int $id
 * @property int $customer_id
 * @property string $type
 * @property int $quantity
 * @property string $unit
 * @property float $price
 * @property int $tax_id
 * @property float $total
 * @property string $date
 * @property string $category
 * @property string $description
 * @property string $period_from
 * @property string $period_to
 * @property int $service_id
 * @property int $payment_id
 * @property int $invoice_id
 * @property int|null $invoiced_by_id
 * @property string $comment
 * @property bool $to_invoice
 * @property string $service_type
 * @property string $source
 * @property int|null $vendor_id
 * @property bool $deleted
 * @property string $updated_at
 */
class ReportDataRow
{
    public const SOURCE_MANUAL = 'manual';
    public const SOURCE_AUTO = 'auto';
    public const SOURCE_CDR = 'cdr';

    public const TYPE_DEBIT = 'debit';
    public const TYPE_CREDIT = 'credit';

    public const SERVICE_TYPE_INTERNET = 'internet';
    public const SERVICE_TYPE_VOICE = 'voice';
    public const SERVICE_TYPE_CUSTOM = 'custom';

    /**
     * @return float|int
     */
    public function calculateTotal()
    {
        return $this->type === self::TYPE_DEBIT ? round((float)$this->total, 2) : round((float)$this->total, 2) * -1;
    }
}
