<?php

namespace app\models\report;

use app\components\FixedCommission;
use app\components\NewCustomerCommission;
use app\helpers\ReportDataHelper;
use app\models\api\Customer;
use app\models\api\Vendor;
use app\models\db\Customers;
use app\models\db\Invoices;
use DateTime;
use DateTimeZone;
use Exception;
use splynx\base\ApiResponseException;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidValueException;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\i18n\Formatter;

/**
 * Class ReportService
 * @package app\models
 *
 * @property ReportDataRepository $reportDataRepository
 * @property array $categories
 */
class ReportService extends Model
{
    /**
     * @var array $_categories Transaction categories for report
     */
    private $_categories;
    /**
     * @var DateTimeZone $_timezone Timezone object for correct dates diffs behavior
     */
    private $_timezone;
    /**
     * @var Formatter
     */
    private $_formatter;

    /**
     * @var ReportDataRepository
     */
    private $_reportDataRepository;

    /**
     * ReportService constructor.
     * @param array $categories
     * @param array $config
     */
    public function __construct(array $categories, $config = [])
    {
        $this->_categories = $categories;
        $this->_timezone = new DateTimeZone('UTC');
        $this->_formatter = Yii::$app->getFormatter();
        $this->_reportDataRepository = new ReportDataRepository();
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->_categories;
    }

    /**
     * @param array $categories
     */
    public function setCategories(array $categories): void
    {
        $this->_categories = $categories;
    }

    /**
     * @param string $fromDate
     * @param string $toDate
     * @param array $vendors
     * @return array
     * @throws \yii\db\Exception
     * @throws Exception
     */
    public function generateVendorsReport(string $fromDate, string $toDate, array $vendors = []): array
    {
        $result = [];
        $customersCount = [];
        $invoices = [];
        $vendorsModels = $this->findVendorsForReport($vendors);
        $categories = $this->getCategories();
        $startDates = $this->_reportDataRepository->findStartDates($vendors, [], $categories);
        $data = $this->_reportDataRepository->findData($fromDate, $toDate, $categories, $vendors);

        $fixedCommission = new FixedCommission($fromDate, ['timezone' => $this->_timezone, 'formatter' => $this->_formatter]);
        $newCustomerCommission = new NewCustomerCommission($fromDate, $toDate, ['timezone' => $this->_timezone, 'formatter' => $this->_formatter]);

        $firstDayOfMonthFilterPeriod = $this->getFirstDayOfMonthDate($fromDate);
        $maxCoveredDates = $this->_reportDataRepository->findMaxCoveredDate($firstDayOfMonthFilterPeriod->format('Y-m-d'), $categories, [], []);

        /** @var ReportDataRow $transaction */
        foreach ($data as $transaction) {
            if ($transaction->vendor_id === null || !array_key_exists($transaction->vendor_id, $vendorsModels)) {
                // Cant find
                continue;
            }
            if (!array_key_exists($transaction->customer_id, $startDates)) {
                throw new InvalidValueException(sprintf('Customer ID(%s) start date error!', $transaction->customer_id));
            }
            $vendor = $vendorsModels[$transaction->vendor_id];
            $vendorPayPeriod = (int)$vendor->additional_attributes[Vendor::PAY_PERIOD];
            $startDate = $startDates[$transaction->customer_id];

            $fixedCommission->setVendorProperties($vendor);

            $fixedCommission->setEndDateByCustomer($vendor->id, $transaction->customer_id, $startDate, $vendorPayPeriod);

            // Calculate months that have been covered; do it only once for each customer
            if (!$fixedCommission->isMonthMapPopulatedForCustomer($vendor->id, $transaction->customer_id)) {
                $fixedCommission->populateCoveredMonthsMapByCustomer($vendor->id, $transaction->customer_id, $fromDate, $maxCoveredDates);
            }
            // Ignore transactions that should be counted by period dates in future periods (outside searched period)
            if ($transaction->period_from !== '0000-00-00' && ($transaction->period_from > $toDate || $transaction->period_to < $fromDate)) {
                continue;
            }
            // Ignore transactions counted in last periods
            if ($transaction->period_from !== '0000-00-00' && $this->getFirstDayOfMonthDate($transaction->period_from) < $this->getFirstDayOfMonthDate($fromDate)) {
                continue;
            }

            if (!$this->isTransactionValidForCustomer($transaction, $startDate, $vendorPayPeriod)) {
                continue;
            }

            $total = $transaction->calculateTotal();

            $this->setPeriodInfinity($vendor, $vendor->additional_attributes[Vendor::PAY_PERIOD]);

            $commission = $this->calculateVendorCommission(
                $transaction,
                $vendor->additional_attributes[Vendor::PERCENT],
                $vendor->additional_attributes[Vendor::PAY_PERIOD],
                $startDate
            );

            $fixedCommission->populateMonthsMapWithTransaction($vendor->id, $transaction->customer_id, $transaction);

            if (!array_key_exists($transaction->vendor_id, $result)) {
                $result[$transaction->vendor_id] = [
                    'vendor_id' => $transaction->vendor_id,
                    'name' => ReportDataHelper::generateVendorLink($vendor->additional_attributes[Vendor::NAME], $fromDate, $toDate, $vendor->id),
                    'active_customers_count' => 1,
                    'total_sum' => 0,
                    'commission_total' => 0,
                ];
            }
            // Populate counters
            $customersCount[$transaction->vendor_id][$transaction->customer_id] = 1;
            $invoiceId = $transaction->invoice_id ?? $transaction->invoiced_by_id;
            $invoices[$transaction->vendor_id][$invoiceId] = 1;
            // Add totals
            $result[$transaction->vendor_id]['active_customers_count'] = count($customersCount[$transaction->vendor_id]);
            $result[$transaction->vendor_id]['total_sum'] += $total;
            $result[$transaction->vendor_id]['commission_total'] = round($result[$transaction->vendor_id]['commission_total'] + $commission, 2);
        }

        foreach ($vendorsModels as $vendor) {
            if (!empty((float)$vendor->additional_attributes[Vendor::NEW_CUSTOMER_PERCENT])) {
                $newCustomerCommission->setVendorProperties($vendor);
                $newCustomerCommission->populateNewCustomers($vendor->id);
                $newCustomerCommission->populateCustomersWithCancelledService($vendor->id);

                if (!$newCustomerCommission->isNewCustomersPopulated($vendor->id)) {
                    continue;
                }
                if (!isset($result[$vendor->id])) {
                    $result[$vendor->id] = [
                        'vendor_id' => $vendor->id,
                        'name' => ReportDataHelper::generateVendorLink($vendor->additional_attributes[Vendor::NAME], $fromDate, $toDate, $vendor->id),
                        'active_customers_count' => 1,
                        'total_sum' => 0,
                        'commission_total' => 0,
                    ];
                }
            }
        }

        // Add unpaid invoices columns
        foreach (array_keys($result) as $vendorId) {
            $unpaidTotal = 0;
            $fixedRow = '';
            if (isset($invoices[$vendorId])) {
                $unpaidInvoices = $this->findUnpaidInvoicesByIds(array_keys($invoices[$vendorId]));
                foreach ($unpaidInvoices as $invoice) {
                    $unpaidTotal += $invoice->total;
                }

                $fixedRow = $fixedCommission->getReportRowForConsolidatedReport($vendorId);
                if ($fixedRow) {
                    $result[$vendorId]['commission_total'] += $fixedRow['amount'];
                }
            }

            $newCustomerRow = $newCustomerCommission->getReportRowForConsolidatedReport($vendorId);
            if ($newCustomerRow) {
                $result[$vendorId]['commission_total'] += $newCustomerRow['amount'];
            }

            $result[$vendorId]['unpaid_invoices_total'] = $this->_formatter->asDecimal($unpaidTotal, 2);
            $result[$vendorId]['total_sum'] = $this->_formatter->asDecimal($result[$vendorId]['total_sum'], 2);
            $result[$vendorId]['commission_total'] = $this->_formatter->asDecimal($result[$vendorId]['commission_total'], 2)
                . ($fixedRow ? $fixedRow['description'] : '')
                . ($newCustomerRow ? $newCustomerRow['description'] : '');
        }

        return $result;
    }

    /**
     * @param string $fromDate
     * @param string $toDate
     * @param int $vendorId
     * @return array
     * @throws ApiResponseException
     * @throws \yii\db\Exception
     * @throws Exception
     */
    public function generateVendorReport(string $fromDate, string $toDate, int $vendorId): array
    {
        $result = [];
        $commissionsByCustomer = [];
        $invoices = [];
        if (!$vendor = $this->findVendorById($vendorId)) {
            return $result;
        }
        $categories = $this->getCategories();
        $startDates = $this->_reportDataRepository->findStartDates([$vendorId], [], $categories);
        $customersNames = $this->_reportDataRepository->findCustomersNames([$vendorId]);
        $data = $this->_reportDataRepository->findData($fromDate, $toDate, $categories, [$vendorId]);

        $fixedCommission = new FixedCommission($fromDate, ['timezone' => $this->_timezone, 'formatter' => $this->_formatter]);
        $fixedCommission->setVendorProperties($vendor);

        $firstDayOfMonthFilterPeriod = $this->getFirstDayOfMonthDate($fromDate);
        $maxCoveredDates = $this->_reportDataRepository->findMaxCoveredDate($firstDayOfMonthFilterPeriod->format('Y-m-d'), $categories, [], []);

        /** @var ReportDataRow $transaction */
        foreach ($data as $transaction) {
            if (!array_key_exists($transaction->customer_id, $startDates)) {
                throw new InvalidValueException(sprintf('Customer ID(%s) start date error!', $transaction->customer_id));
            }
            $startDate = $startDates[$transaction->customer_id];
            $vendorPayPeriod = (int)$vendor->additional_attributes[Vendor::PAY_PERIOD];

            $fixedCommission->setEndDateByCustomer($vendor->id, $transaction->customer_id, $startDate, $vendorPayPeriod);

            // Calculate months that have been covered; do it only once for each customer
            if (!$fixedCommission->isMonthMapPopulatedForCustomer($vendor->id, $transaction->customer_id)) {
                $fixedCommission->populateCoveredMonthsMapByCustomer($vendor->id, $transaction->customer_id, $fromDate, $maxCoveredDates);
            }
            // Ignore transactions that should be counted by period dates in future periods (outside searched period)
            if ($transaction->period_from !== '0000-00-00' && ($transaction->period_from > $toDate || $transaction->period_to < $fromDate)) {
                continue;
            }
            // Ignore transactions counted in last periods
            if ($transaction->period_from !== '0000-00-00' && $this->getFirstDayOfMonthDate($transaction->period_from) < $this->getFirstDayOfMonthDate($fromDate)) {
                continue;
            }

            if (!$this->isTransactionValidForCustomer($transaction, $startDate, $vendorPayPeriod)) {
                continue;
            }

            $total = $transaction->calculateTotal();

            $this->setPeriodInfinity($vendor, $vendor->additional_attributes[Vendor::PAY_PERIOD]);

            $commission = $this->calculateVendorCommission(
                $transaction,
                $vendor->additional_attributes[Vendor::PERCENT],
                $vendor->additional_attributes[Vendor::PAY_PERIOD],
                $startDate
            );

            $fixedCommission->populateMonthsMapWithTransaction($vendor->id, $transaction->customer_id, $transaction);

            if (!array_key_exists($transaction->customer_id, $result)) {
                $name = ArrayHelper::getValue($customersNames, (string)$transaction->customer_id, '---');
                $result[$transaction->customer_id] = [
                    'customer_id' => $transaction->customer_id,
                    'customer_name' => ReportDataHelper::generateCustomerLink($name, $fromDate, $toDate, $transaction->customer_id),
                    'transactions_count' => 0,
                    'total_sum' => 0,
                    'commission_total' => 0,
                ];
            }
            ++$result[$transaction->customer_id]['transactions_count'];
            $result[$transaction->customer_id]['total_sum'] += $total;
            $result[$transaction->customer_id]['commission_total'] += $commission;
            $invoiceId = $transaction->invoice_id ?? $transaction->invoiced_by_id;
            $invoices[$transaction->customer_id][$invoiceId] = 1;
        }

        // Add new customer percent
        if ($vendor->isAllowedNewCustomerCommission()) {
            $newCustomerCommission = new NewCustomerCommission($fromDate, $toDate, ['timezone' => $this->_timezone, 'formatter' => $this->_formatter]);
            $newCustomerCommission->setVendorProperties($vendor);
            $newCustomerCommission->populateNewCustomers($vendor->id);
            $newCustomerCommission->populateCustomersWithCancelledService($vendor->id);
            $result = $newCustomerCommission->mergeNewCustomersToVendorReport($vendor->id, $result);
        }

        foreach (array_keys($result) as $customerId) {
            $invoicesIds = [];
            $unpaidTotal = 0;
            if (isset($invoices[$customerId])) {
                $unpaidInvoices = $this->findUnpaidInvoicesByIds(array_keys($invoices[$customerId]));
                foreach ($unpaidInvoices as $invoice) {
                    $invoicesIds[] = $invoice->id;
                    $unpaidTotal += $invoice->total;
                }
            }

            $commissionsByCustomer[$customerId] = $result[$customerId]['commission_total'];
            $fixedCommissionEntity = $fixedCommission->getCommissionByCustomerForVendorReport($vendor->id, $customerId);
            if (!empty($fixedCommissionEntity['amount'])) {
                $commissionsByCustomer[$customerId] += $fixedCommissionEntity['amount'];
            }
            if ($vendor->isAllowedNewCustomerCommission()) {
                $newCustomerCommissionEntity = $newCustomerCommission->getCommissionByCustomer($vendor->id, $customerId);
                if (!empty($newCustomerCommissionEntity['amount'])) {
                    $commissionsByCustomer[$customerId] += $newCustomerCommissionEntity['amount'];
                }
                $newCustomerNegativeCommissionEntity = $newCustomerCommission->getNegativeCommissionByCustomer($vendor->id, $customerId);
                if (!empty($newCustomerNegativeCommissionEntity['negative_amount'])) {
                    $commissionsByCustomer[$customerId] += $newCustomerNegativeCommissionEntity['negative_amount'];
                }
            }

            $result[$customerId]['unpaid_invoices_ids'] = $invoicesIds;
            $result[$customerId]['unpaid_invoices_total'] = $this->_formatter->asDecimal($unpaidTotal, 2);
            $result[$customerId]['total_sum'] = $this->_formatter->asDecimal($result[$customerId]['total_sum'], 2);
            $result[$customerId]['commission_total'] = $this->_formatter->asDecimal($commissionsByCustomer[$customerId], 2)
                . (!empty($fixedCommissionEntity['description']) ? '<br>' . $fixedCommissionEntity['description'] : '')
                . (!empty($newCustomerCommissionEntity['description']) ? '<br>' . $newCustomerCommissionEntity['description'] : '')
                . (!empty($newCustomerNegativeCommissionEntity['negative_description']) ? '<br>' . $newCustomerNegativeCommissionEntity['negative_description'] : '');
        }

        return $result;
    }

    /**
     * @param string $fromDate
     * @param string $toDate
     * @param int $customerId
     * @return array
     * @throws ApiResponseException
     * @throws \yii\db\Exception
     * @throws Exception
     */
    public function generateCustomerReport(string $fromDate, string $toDate, int $customerId): array
    {
        $result = [];
        $customer = $this->findCustomerById($customerId);
        if (!$customer || empty($customer->additional_attributes[Vendor::ADD_ON_ENTITY])) {
            return $result;
        }
        if (!$vendor = $this->findVendorById((int)$customer->additional_attributes[Vendor::ADD_ON_ENTITY])) {
            return $result;
        }

        if ($vendor->isAllowedNewCustomerCommission()) {
            $newCustomerCommission = new NewCustomerCommission($fromDate, $toDate, ['timezone' => $this->_timezone, 'formatter' => $this->_formatter]);
            $newCustomerCommission->setVendorProperties($vendor);
            $newCustomerCommission->populateNewCustomers($vendor->id, $customerId);
            $newCustomerCommission->populateCustomersWithCancelledService($vendor->id, $customerId);
        }

        $categories = $this->getCategories();
        $startDates = $this->_reportDataRepository->findStartDates([], [$customerId], $categories);

        if (!array_key_exists($customerId, $startDates) && (!$vendor->isAllowedNewCustomerCommission() || !$newCustomerCommission->isNewCustomersPopulated($vendor->id))) {
            return $result;
        }

        if (isset($startDates[$customerId])) {
            // Standard and fixed commission
            $startDate = $startDates[$customerId];

            // No result before customer start date
            if (($toDate < $startDate) && (!$vendor->isAllowedNewCustomerCommission() || !$newCustomerCommission->isNewCustomersPopulated($vendor->id))) {
                return $result;
            }

            $vendorPayPeriod = (int)$vendor->additional_attributes[Vendor::PAY_PERIOD];
            $vendorPayPeriodEndDate = $this->calculateValidEndPeriod($startDate, $toDate, $vendorPayPeriod);

            $data = $this->_reportDataRepository->findData($fromDate, $vendorPayPeriodEndDate, $categories, [], [$customerId]);

            $fixedCommission = new FixedCommission($fromDate, ['timezone' => $this->_timezone, 'formatter' => $this->_formatter]);
            $fixedCommission->setVendorProperties($vendor);
            $fixedCommission->setEndDateByCustomer($vendor->id, $customerId, $startDate, $vendorPayPeriod);

            $firstDayOfMonthFilterPeriod = $this->getFirstDayOfMonthDate($fromDate);
            $maxCoveredDates = $this->_reportDataRepository->findMaxCoveredDate($firstDayOfMonthFilterPeriod->format('Y-m-d'), $categories, [], [$customerId]);
            $fixedCommission->populateCoveredMonthsMapByCustomer($vendor->id, $customerId, $fromDate, $maxCoveredDates);

            /** @var ReportDataRow $transaction */
            foreach ($data as $transaction) {
                // Ignore transactions that should be counted by period dates in future periods (outside searched period)
                if ($transaction->period_from !== '0000-00-00' && ($transaction->period_from > $toDate || $transaction->period_to < $fromDate)) {
                    continue;
                }
                // Ignore transactions counted in last periods
                if ($transaction->period_from !== '0000-00-00' && $this->getFirstDayOfMonthDate($transaction->period_from) < $this->getFirstDayOfMonthDate($fromDate)) {
                    continue;
                }
                if (!$this->isTransactionValidForCustomer($transaction, $startDate, $vendorPayPeriod)) {
                    continue;
                }

                $total = $transaction->calculateTotal();

                $this->setPeriodInfinity($vendor, $vendor->additional_attributes[Vendor::PAY_PERIOD]);

                $commission = $this->calculateVendorCommission(
                    $transaction,
                    $vendor->additional_attributes[Vendor::PERCENT],
                    $vendor->additional_attributes[Vendor::PAY_PERIOD],
                    $startDate
                );

                $fixedCommission->populateMonthsMapWithTransaction($vendor->id, $customerId, $transaction);

                $result[$transaction->id] = [
                    'billing_transactions_id' => $transaction->id,
                    'category' => $transaction->category,
                    'description' => $transaction->description,
                    'date' => $this->_formatter->asDate($transaction->date),
                    'period_from' => $transaction->period_from !== '0000-00-00' ? $this->_formatter->asDate($transaction->period_from) : '',
                    'period_to' => $transaction->period_from !== '0000-00-00' ? $this->_formatter->asDate($transaction->period_to) : '',
                    'invoice_id' => $transaction->invoice_id ?? $transaction->invoiced_by_id,
                    'total' => $this->_formatter->asDecimal($total, 2),
                    'commission' => $this->_formatter->asDecimal($commission, 2),
                ];
            }

            $fixedCommissionRow = $fixedCommission->getReportRowForCustomerReport($vendor->id, $customerId);
            if ($fixedCommissionRow) {
                $result['fixed-commission'] = $fixedCommissionRow;
            }
        }

        // Add new customer percent
        if ($vendor->isAllowedNewCustomerCommission()) {
            $newCustomerCommissionRow = $newCustomerCommission->getReportRowForCustomerReport($vendor->id, $customerId);
            if ($newCustomerCommissionRow) {
                $result['new-customer'] = $newCustomerCommissionRow;
            }
            $newCustomerNegativeCommissionRow = $newCustomerCommission->getReportNegativeRowForCustomerReport($vendor->id, $customerId);
            if ($newCustomerNegativeCommissionRow) {
                $result['cancelled-service'] = $newCustomerNegativeCommissionRow;
            }
        }

        return $this->correctedCommission($result);
    }

    /**
     * @param array $result
     * @return array
     */
    protected function correctedCommission(array $result): array
    {
        $corrected = $this->getIdOfWrongCommission($result);
        foreach ($result as $transactionKey => $transactionId) {
            if (in_array($transactionId['billing_transactions_id'], $corrected)) {
                $result[$transactionKey]['commission'] = $this->_formatter->asDecimal(0, 2);
            }
        }
        return $result;
    }

    /**
     * @param array $result
     * @return array
     */
    protected function getIdOfWrongCommission(array $result): array
    {
        $commission = [];
        foreach ($this->searchDuplicate($result) as $arrayControl) {
            $max = 0;
            foreach ($arrayControl as $invoice => $amount) {
                if (!empty($max) && $max > $amount) {
                    $commission[] = $invoice;
                }
                $max = $amount;
            }
        }
        return $commission;
    }

    /**
     * @param array $array
     * @return array
     */
    protected function searchDuplicate(array $array): array
    {
        $invoiceIds = [];
        foreach ($array as $duplicate) {
            $invoiceIds[$duplicate['invoice_id']][$duplicate['billing_transactions_id']] = $duplicate['total'];
        }
        return array_filter($invoiceIds, static function ($invoiceId) {
            return 1 !== count($invoiceId);
        });
    }

    /**
     * @param ReportDataRow $transaction
     * @param float $commissionPercent
     * @param integer $vendorPeriods
     * @param string $startPeriod
     * @return float
     * @throws Exception
     */
    protected function calculateVendorCommission(ReportDataRow $transaction, float $commissionPercent, int $vendorPeriods, string $startPeriod): float
    {
        // Validate invalid params cases
        if ($transaction->period_from === '0000-00-00' && $startPeriod > $transaction->date) {
            throw new InvalidArgumentException('Transaction date is older than start date!');
        }
        if ($transaction->period_to !== '0000-00-00' && $startPeriod > $transaction->period_to) {
            throw new InvalidArgumentException('Transaction period is older than start date!');
        }

        $targetPeriod = $transaction->period_to === '0000-00-00' ? $transaction->date : $transaction->period_to;
        $endPeriod = $this->calculateValidEndPeriod($startPeriod, $targetPeriod, $vendorPeriods);
        if ($vendorPeriods !== Vendor::PAY_PERIOD_INFINITE) {
            if ($transaction->period_from === '0000-00-00' && $endPeriod < $transaction->date) {
                throw new InvalidArgumentException('Transaction date is out of commission periods range! Transaction date: ' . $transaction->date . ' End date: ' . $endPeriod);
            }
            if ($transaction->period_to !== '0000-00-00' && $endPeriod < $transaction->period_from) {
                throw new InvalidArgumentException('Transaction date is out of commission periods range! wtf');
            }
        }

        // Calculate for 1 period transaction case
        if ($transaction->period_from === '0000-00-00') {
            return round(($transaction->calculateTotal() * ($commissionPercent / 100)), 2);
        }

        // Calculate commissioned range in days
        $transactionPeriodFrom = new DateTime($transaction->period_from, $this->_timezone);
        $transactionPeriodTo = new DateTime($transaction->period_to, $this->_timezone);
        $transactionDaysInterval = $transactionPeriodTo->diff($transactionPeriodFrom);
        $transactionDays = (int)$transactionDaysInterval->format('%a') + 1;

        $startPeriodDate = $this->getFirstDayOfMonthDate($startPeriod);
        $payFromDate = $transactionPeriodFrom < $startPeriodDate ? $startPeriodDate : $transactionPeriodFrom;
        $payToDate = new DateTime($endPeriod, $this->_timezone);
        $payForDaysInterval = $payToDate->diff($payFromDate);
        $payForDays = (int)$payForDaysInterval->format('%a') + 1;

        // Calculate for valid commissioned days only
        return round((($transaction->calculateTotal() / $transactionDays * $payForDays) * ($commissionPercent / 100)), 2);
    }

    /**
     * Checks if transaction is in customer commissioned range
     * @param ReportDataRow $transaction
     * @param string $startPeriod
     * @param int $vendorPeriods
     * @return bool
     * @throws Exception
     */
    protected function isTransactionValidForCustomer(ReportDataRow $transaction, string $startPeriod, int $vendorPeriods): bool
    {
        // Validate invalid params cases
        if ($transaction->period_from === '0000-00-00' && $startPeriod > $transaction->date) {
            return false;
        }
        if ($transaction->period_to !== '0000-00-00' && $startPeriod > $transaction->period_to) {
            return false;
        }

        if ($vendorPeriods === Vendor::PAY_PERIOD_INFINITE) {
            return true;
        }

        $targetPeriod = $transaction->period_to === '0000-00-00' ? $transaction->date : $transaction->period_to;
        $endPeriod = $this->calculateValidEndPeriod($startPeriod, $targetPeriod, $vendorPeriods);
        if ($transaction->period_from === '0000-00-00' && $endPeriod < $transaction->date) {
            return false;
        }
        if ($transaction->period_to !== '0000-00-00' && $endPeriod < $transaction->period_from) {
            return false;
        }

        return true;
    }

    /**
     * Calculates how many periods left to pay commission for customer
     * @param string $startPeriod Customer start period Y-m-d
     * @param string $targetPeriod Currently processed period
     * @param integer $periods Vendor periods to pay
     * @return int Return 0 when there is no more pay periods for customer
     * @throws Exception
     */
    protected function calculatePeriodsLeft(string $startPeriod, string $targetPeriod, int $periods): int
    {
        $startPeriodDate = $this->getFirstDayOfMonthDate($startPeriod);
        $targetPeriodDate = $this->getFirstDayOfMonthDate($targetPeriod);
        $interval = $targetPeriodDate->diff($startPeriodDate);
        $monthsBetweenPeriods = $interval->m + (12 * $interval->y);
        $periodsLeft = $periods - $monthsBetweenPeriods;

        return $periodsLeft >= 0 ? $periodsLeft : 0;
    }

    /**
     * Validates and returns valid end date for customer
     * @param string $startPeriod Customer starting period 'Y-m-d'
     * @param string $targetPeriod End date to check 'Y-m-d'
     * @param int $periods Paid periods
     * @return string 'Y-m-d' formatted date
     * @throws Exception
     */
    protected function calculateValidEndPeriod(string $startPeriod, string $targetPeriod, int $periods): string
    {
        $startPeriodDate = $this->getFirstDayOfMonthDate($startPeriod);
        $targetPeriodDate = new DateTime($targetPeriod, $this->_timezone);
        $targetDay = $targetPeriodDate->format('d');
        $targetPeriodDate->modify('first day of this month');

        if ($targetPeriodDate < $startPeriodDate) {
            $startPeriodDate->modify('last day of this month');
            $lastMonthday = $startPeriodDate->format('d');
            $endDay = $targetDay > $lastMonthday ? $lastMonthday : $targetDay;

            return $startPeriodDate->format('Y-m-' . $endDay);
        }

        if ($periods === Vendor::PAY_PERIOD_INFINITE) {
            return $targetPeriod;
        }

        $interval = $targetPeriodDate->diff($startPeriodDate);
        $monthsBetweenPeriods = $interval->m + (12 * $interval->y);
        $periodsLeft = $periods - $monthsBetweenPeriods;

        if ($periodsLeft > 0) {
            return $targetPeriod;
        }
        // 1 period it is same month
        if ($periods !== 1) {
            $addPeriods = $periods - 1;
            $startPeriodDate->modify("+ $addPeriods month");
        }
        return $startPeriodDate->format('Y-m-t');
    }

    /**
     * @param string $date 'Y-m-d'
     * @return DateTime
     * @throws Exception
     */
    protected function getFirstDayOfMonthDate(string $date): DateTime
    {
        $result = new DateTime($date, $this->_timezone);
        $result->modify('first day of this month');

        return $result;
    }

    /**
     * @param array $ids
     * @return Vendor[]
     */
    protected function findVendorsForReport(array $ids = []): array
    {
        return Vendor::findForReport($ids);
    }

    /**
     * @param int $id
     * @return Vendor|null
     * @throws ApiResponseException
     */
    protected function findVendorById(int $id): ?Vendor
    {
        return Vendor::findByIdStatic($id);
    }

    /**
     * @param array $ids
     * @return Invoices[]
     */
    protected function findUnpaidInvoicesByIds(array $ids): array
    {
        return Invoices::findUnpaidByIds($ids);
    }

    /**
     * @param int $id
     * @return Customers|null
     */
    protected function findCustomersById(int $id): ?Customers
    {
        return Customers::findById($id);
    }

    /**
     * @param int $id
     * @return Customer|null
     * @throws ApiResponseException
     */
    protected function findCustomerById(int $id): ?Customer
    {
        return Customer::findByIdStatic($id);
    }

    /**
     * @return ReportDataRepository
     */
    public function getReportDataRepository(): ReportDataRepository
    {
        return $this->_reportDataRepository;
    }

    /**
     * @param ReportDataRepository $reportDataRepository
     */
    public function setReportDataRepository(ReportDataRepository $reportDataRepository): void
    {
        $this->_reportDataRepository = $reportDataRepository;
    }

    /**
     * @param Vendor $vendor
     * @param string $vendorPayPeriod
     * @return void
     */
    public function setPeriodInfinity(Vendor $vendor, string $vendorPayPeriod): void
    {
        if ((int)$vendorPayPeriod == trim((int)$vendorPayPeriod) && strpos($vendorPayPeriod, ' ') !== false) {
            $vendor->additional_attributes[Vendor::PAY_PERIOD] = Vendor::PAY_PERIOD_INFINITE;
        }
    }
}
