<?php

namespace app\models\report;

use app\components\NewCustomerCommission;
use app\models\api\Vendor;
use PDO;
use Yii;
use yii\base\Model;
use yii\db\Connection;
use yii\db\DataReader;
use yii\db\Exception;
use yii\helpers\ArrayHelper;

/**
 * Class ReportDataRepository
 * Contains methods to get reports required data
 * @package app\models\report
 */
class ReportDataRepository extends Model
{
    /**
     * @param string $fromDate
     * @param string $toDate
     * @param array $categories
     * @param array $vendors
     * @param array $customers
     * @return array|DataReader
     * @throws Exception
     */
    public function findData(string $fromDate, string $toDate, array $categories, $vendors = [], $customers = [])
    {
        if (empty($categories)) {
            return [];
        }
        $ids = "'" . implode("','", $categories) . "'";
        $addOnEntity = "'" . Vendor::ADD_ON_ENTITY . "'";
        $params = [
            ':start_date' => $fromDate,
            ':end_date' => $toDate,
        ];
        $sql = "
        SELECT billing_transactions.*, customers_values.value as vendor_id
        FROM billing_transactions
        RIGHT JOIN customers_values
            ON billing_transactions.customer_id = customers_values.id
            AND customers_values.name = {$addOnEntity}
            AND customers_values.value <> ''
        WHERE 
           (date BETWEEN :start_date AND :end_date OR (period_from <= :end_date AND period_to >= :start_date))
           AND deleted = '0'
           AND category IN ($ids)";
        if ($vendors) {
            $vendorsIds = "'" . implode("','", $vendors) . "'";
            $sql .= " AND customers_values.value IN ($vendorsIds)";
        }

        if ($customers) {
            $customersIds = "'" . implode("','", $customers) . "'";
            $sql .= " AND customer_id IN ($customersIds)";
        }
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql, $params);

        $dataReader = $command->query();
        $dataReader->setFetchMode(PDO::FETCH_CLASS, ReportDataRow::class);

        return $dataReader;
    }

    /**
     * @param array $vendors
     * @param array $customers
     * @param array<int, int> $categories
     * @return array
     * @throws Exception
     */
    public function findStartDates(array $vendors = [], array $customers = [], array $categories = []): array
    {
        if (empty($categories)) {
            return [];
        }

        $addOnEntity = "'" . Vendor::ADD_ON_ENTITY . "'";

        $categoryIds = "'" . implode("','", $categories) . "'";
        $sql = <<<SQL
        SELECT customer_id, MIN(date) as date
        FROM billing_transactions
        RIGHT JOIN customers_values
            ON billing_transactions.customer_id = customers_values.id
            AND customers_values.name = {$addOnEntity}
            AND customers_values.value <> ''
        WHERE deleted = '0'
        AND category IN ({$categoryIds})
SQL;
        $vendorsSqlPart = '';
        if ($vendors) {
            $vendorsIds = "'" . implode("','", $vendors) . "'";
            $vendorsSqlPart .= " AND customers_values.value IN ($vendorsIds)";
        }

        $customersSqlPart = '';
        if ($customers) {
            $customersIds = "'" . implode("','", $customers) . "'";
            $customersSqlPart .= " AND customer_id IN ($customersIds)";
        }
        $groupByPart = ' GROUP BY customer_id';
        $sql .= $vendorsSqlPart . $customersSqlPart . $groupByPart;

        $sqlPeriods = <<<SQL
        SELECT customer_id, MIN(period_from) as period_from
        FROM billing_transactions
        RIGHT JOIN customers_values
            ON billing_transactions.customer_id = customers_values.id
            AND customers_values.name = {$addOnEntity}
            AND customers_values.value <> ''
        WHERE deleted = '0' AND period_from <> '0000-00-00' AND category IN ({$categoryIds})
SQL;
        $sqlPeriods .= $vendorsSqlPart . $customersSqlPart . $groupByPart;

        $dates = ArrayHelper::map($this->runCommandAndGetData($sql), 'customer_id', 'date');
        $periods = ArrayHelper::map($this->runCommandAndGetData($sqlPeriods), 'customer_id', 'period_from');

        foreach ($dates as $id => $date) {
            if (($period = ArrayHelper::getValue($periods, $id)) && $period < $date) {
                $dates[$id] = $period;
            }
        }

        // There can be all customers
        return $dates;
    }

    /**
     * @param string $sql
     * @return array<int, array<mixed>>
     * @throws Exception
     */
    public function runCommandAndGetData(string $sql): array
    {
        /** @var Connection $connection */
        $connection = Yii::$app->getDb();
        return $connection->createCommand($sql)->queryAll();
    }

    /**
     * @param array $vendors
     * @param array $customers
     * @return array
     * @throws Exception
     */
    public function findCustomersNames(array $vendors = [], array $customers = []): array
    {
        $params = [];
        $addOnEntity = "'" . Vendor::ADD_ON_ENTITY . "'";

        $sql = <<<SQL
        SELECT customers.id, customers.name
        FROM customers
        RIGHT JOIN customers_values
            ON customers.id = customers_values.id
            AND customers_values.name = {$addOnEntity}
            AND customers_values.value <> ''
        WHERE deleted = '0'
SQL;
        if ($vendors) {
            $vendorsIds = "'" . implode("','", $vendors) . "'";
            $sql .= " AND customers_values.value IN ($vendorsIds)";
        }

        if ($customers) {
            $customersIds = "'" . implode("','", $customers) . "'";
            $sql .= " AND customers.id IN ($customersIds)";
        }
        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql, $params);

        // There can be all customers
        return ArrayHelper::map($command->queryAll(), 'id', 'name');
    }

    /**
     * find max period_to transaction date for transactions that started erlier than $toDate but last later
     * @param string $toDate
     * @param array $categories
     * @param array $vendors
     * @param array $customers
     * @return array
     */
    public function findMaxCoveredDate(string $toDate, array $categories, $vendors = [], $customers = [])
    {
        if (empty($categories)) {
            return [];
        }
        $ids = "'" . implode("','", $categories) . "'";
        $addOnEntity = "'" . Vendor::ADD_ON_ENTITY . "'";
        $params = [
            ':end_date' => $toDate,
        ];
        $sql = "
        SELECT customer_id, MAX(period_to) as max_date
        FROM billing_transactions
        RIGHT JOIN customers_values
            ON billing_transactions.customer_id = customers_values.id
            AND customers_values.name = {$addOnEntity}
            AND customers_values.value <> ''
        WHERE 
           period_from < :end_date AND period_to >= :end_date
           AND deleted = '0'
           AND category IN ($ids)";

        $vendorsSqlPart = '';
        if ($vendors) {
            $vendorsIds = "'" . implode("','", $vendors) . "'";
            $vendorsSqlPart .= " AND customers_values.value IN ($vendorsIds)";
        }

        $customersSqlPart = '';
        if ($customers) {
            $customersIds = "'" . implode("','", $customers) . "'";
            $customersSqlPart .= " AND customer_id IN ($customersIds)";
        }
        $groupByPart = ' GROUP BY customer_id';
        $sql .= $vendorsSqlPart . $customersSqlPart . $groupByPart;

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql, $params);

        return ArrayHelper::map($command->queryAll(), 'customer_id', 'max_date');
    }

    /**
     * find customers who were converted from lead
     * @param string $startDate
     * @param string $toDate
     * @param array $vendors
     * @param int|null $customerId
     * @return array
     */
    public function findNewCustomers(string $startDate, string $toDate, $vendors = [], $customerId = null)
    {
        $vendorsIds = "'" . implode("','", $vendors) . "'";
        $addOnEntity = "'" . Vendor::ADD_ON_ENTITY . "'";
        $params = [
            ':start_date' => $startDate,
            ':end_date' => $toDate,
            ':start_date_pattern' => substr($startDate, 0, 7) . '%',
        ];
        if (!empty($customerId)) {
            $params[':customerId'] = $customerId;
        }

        $sql = "
        SELECT A.id, A.name, A.conversion_date,
            B.quantity, B.unit_price, B.discount, B.discount_value, 
            B.discount_type, B.discount_start_date, B.discount_end_date
        FROM customers A
        INNER JOIN services_internet B ON B.customer_id = A.id
        INNER JOIN customers_values C ON C.id = A.id
            AND C.name = {$addOnEntity}
            AND C.value IN ($vendorsIds)
        WHERE
            A.deleted = '0' AND B.deleted = '0'
            AND (
                (conversion_date >= :start_date AND conversion_date <= :end_date) 
                OR (conversion_date like :start_date_pattern)
            )
            " . (!empty($customerId) ? ' AND A.id = :customerId' : '');

        $connection = Yii::$app->getDb();
        return $connection->createCommand($sql, $params)->queryAll();
    }

    /**
     * find customers whose services were stopped been active in 6 month after converting from lead
     * @param string $startDate
     * @param string $toDate
     * @param array $vendors
     * @param int|null $customerId
     * @param int $cancellationPeriod
     * @return array
     */
    public function findCustomersWithCancelledService(
        string $startDate,
        string $toDate,
        $vendors = [],
        $customerId = null,
        $cancellationPeriod = 6
    ) {
        $vendorsIds = "'" . implode("','", $vendors) . "'";
        $addOnEntity = "'" . Vendor::ADD_ON_ENTITY . "'";
        $params = [
            ':start_date' => $startDate,
            ':end_date' => $toDate,
            ':cancellation_period' => $cancellationPeriod,
            ':adf_amount' => NewCustomerCommission::ADF_AMOUNT,
            ':adf_cancellation_date' => NewCustomerCommission::ADF_CANCELLATION_DATE,
        ];
        if (!empty($customerId)) {
            $params[':customerId'] = $customerId;
        }

        $sql = "
        SELECT A.id, A.name, 
            C2.value AS service_amount, C3.value as date_cancelled
        FROM customers A
        INNER JOIN customers_values C ON C.id = A.id
            AND C.name = {$addOnEntity}
            AND C.value IN ($vendorsIds)
        INNER JOIN customers_values C2 ON C2.id = A.id
            AND C2.name = :adf_amount
        INNER JOIN customers_values C3 ON C3.id = A.id
            AND C3.name = :adf_cancellation_date
        WHERE 
            str_to_date(C3.value, '%Y-%m-%d') >= :start_date AND str_to_date(C3.value, '%Y-%m-%d') <= :end_date
            AND TIMESTAMPDIFF(MONTH, A.conversion_date, str_to_date(C3.value, '%Y-%m-%d')) < :cancellation_period
        " . (!empty($customerId) ? ' AND A.id = :customerId' : '');

        $connection = Yii::$app->getDb();
        $command = $connection->createCommand($sql, $params);

        return $command->queryAll();
    }
}
