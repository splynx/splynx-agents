<?php

namespace app\models\db;

use splynx\v2\models\finance\BaseInvoice;

/**
 * Class Customers
 *
 * @property integer $id
 * @property integer $customer_id
 * @property string $number
 * @property string $date_created
 * @property string $real_create_datetime
 * @property string $date_updated
 * @property string $date_payment
 * @property string $date_till
 * @property float $total
 * @property string $status
 * @property integer $payment_id
 * @property string $is_sent
 * @property string $payd_from_deposit
 * @property string $use_transactions
 * @property string $note
 * @property string $memo
 * @property string $added_by
 * @property integer $added_by_id
 * @property string $deleted
 * @package app\models\db
 */
class Invoices extends BaseActiveRecordModel
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{invoices}}';
    }

    /**
     * @param array $ids
     * @return Invoices[]
     */
    public static function findUnpaidByIds(array $ids): array
    {
        /** @var Invoices[] $result */
        $result = static::find()->where(['not', ['status' => BaseInvoice::STATUS_PAID]])->andWhere(['id' => $ids, 'deleted' => '0'])->all();
        return $result;
    }
}
