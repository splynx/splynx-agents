<?php

namespace app\models\db;

use yii\base\NotSupportedException;
use yii\db\ActiveRecord;

/**
 * Class BaseActiveRecordModel
 * @package app\models\db
 */
abstract class BaseActiveRecordModel extends ActiveRecord
{
    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @throws NotSupportedException
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        throw new NotSupportedException('Method not implemented in ' . static::class);
    }

    /**
     * @throws NotSupportedException
     */
    public function delete()
    {
        throw new NotSupportedException('Method not implemented in ' . static::class);
    }

    /**
     * @param null  $condition
     * @param array $params
     *
     * @throws NotSupportedException
     */
    public static function deleteAll($condition = null, $params = [])
    {
        throw new NotSupportedException('Method not implemented in ' . static::class);
    }
}
