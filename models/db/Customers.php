<?php

namespace app\models\db;

/**
 * Class Customers
 *
 * @property integer $id
 * @property string $billing_type
 * @property integer $partner_id
 * @property integer $location_id
 * @property string $added_by
 * @property integer $added_by_id
 * @property string $login
 * @property string $category
 * @property string $password
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $street_1
 * @property string $zip_code
 * @property string $city
 * @property string $status
 * @property string $date_add
 * @property string $last_online
 * @property string $last_update
 * @property string $daily_prepaid_cost
 * @property string $gps
 * @package app\models\db
 */
class Customers extends BaseActiveRecordModel
{
    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return '{{customers}}';
    }

    /**
     * @param integer $id
     * @return Customers|null
     */
    public static function findById(int $id): ?Customers
    {
        return static::findOne(['id' => $id]);
    }
}
