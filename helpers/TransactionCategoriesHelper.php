<?php

namespace app\helpers;

/**
 * Class TransactionCategoriesHelper
 * Base transaction categories
 * @package app\helpers
 */
class TransactionCategoriesHelper
{
    public const SERVICE = 1;
    public const DISCOUNT = 2;
    public const PAYMENT = 3;
    public const REFUND = 4;
    public const CORRECTION = 5;
}
