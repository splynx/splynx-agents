<?php

namespace app\helpers;

use DateTime;
use DateTimeZone;
use Exception;
use splynx\helpers\ConfigHelper;
use splynx\helpers\DateHelper as BaseDateHelper;

/**
 * Class DateHelper
 * @package app\helpers
 */
class DateHelper extends BaseDateHelper
{
    /**
     * Get date with milliseconds
     * @param string $format Date format
     * @return string|null Date string or null on error
     */
    public static function getDateWithMilliseconds($format = 'Y-m-d H:i:s.u'): ?string
    {
        try {
            $date = DateTime::createFromFormat('U.u', number_format(microtime(true), 6, '.', ''));
            $timezone = new DateTimeZone(ConfigHelper::getSplynxTimeZone());
            if ($date && $timezone) {
                $date->setTimezone($timezone);

                return $date->format($format);
            }
        } catch (Exception $exception) {
        }

        return null;
    }
}
