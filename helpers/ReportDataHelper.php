<?php

namespace app\helpers;

use app\models\api\Vendor;
use splynx\helpers\DateHelper;
use yii\bootstrap\Html;
use DateTime;

class ReportDataHelper
{
    /** @var string */
    public const BASE_URL = '/agents';
    /** @var string */
    protected static $_systemDateFormat;

    /**
     * @param string $text
     * @param string $fromDate
     * @param string $toDate
     * @param int $id
     * @return string
     */
    public static function generateCustomerLink(string $text, string $fromDate, string $toDate, int $id): string
    {
        $from = new DateTime($fromDate);
        $to = new DateTime($toDate);
        $period = $from->format(static::getCustomSplynxDateFormat()) . ' - ' . $to->format(static::getCustomSplynxDateFormat());

        return Html::a($text, static::BASE_URL . '/customer-report?customer=' . $id . '&period=' . $period);
    }

    /**
     * @param string $text
     * @param string $fromDate
     * @param string $toDate
     * @param int $id
     * @return string
     */
    public static function generateVendorLink(string $text, string $fromDate, string $toDate, int $id): string
    {
        $from = new DateTime($fromDate);
        $to = new DateTime($toDate);
        $period = $from->format(static::getCustomSplynxDateFormat()) . ' - ' . $to->format(static::getCustomSplynxDateFormat());

        return Html::a($text, static::BASE_URL . '/vendor-report?vendor=' . $id . '&period=' . $period);
    }

    /**
     * In order to match the date from the Splynx settings with the test
     *
     * @param string $format
     * @return void
     */
    public static function setCustomSplynxDateFormat(string $format): void
    {
        if (empty($format)) {
            return;
        }

        static::$_systemDateFormat = $format;
    }

    /**
     * @return string
     */
    protected static function getCustomSplynxDateFormat(): string
    {
        if (static::$_systemDateFormat === null) {
            static::$_systemDateFormat = DateHelper::getSplynxDateFormat();
        }

        return static::$_systemDateFormat;
    }
}
