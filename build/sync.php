#!/usr/bin/php
<?php

$currentReleaseVersion = null;
include_once __DIR__ . '/config.php';

if ($currentReleaseVersion === null) {
    echo "Can't get current version!\n";
    exit();
}

exec('git checkout master', $output, $resultCode);
if ($resultCode !== 0) {
    echo "Can't checkout to master!\n";
    exit();
}

exec('git pull', $output, $resultCode);
if ($resultCode !== 0) {
    echo "Can't make pull changes in master!\n";
    exit();
}

exec("git checkout release-{$currentReleaseVersion}", $output, $resultCode);
if ($resultCode !== 0) {
    echo "Can't checkout to release-{$currentReleaseVersion}!\n";
    exit();
}

exec('git pull', $output, $resultCode);
if ($resultCode !== 0) {
    echo "Can't pull changes in release-{$currentReleaseVersion}!\n";
    exit();
}

exec('git merge master', $output, $resultCode);
if ($resultCode !== 0) {
    echo "Can't merge master!\n";
    exit();
}
exec('git push', $output, $resultCode);
if ($resultCode !== 0) {
    echo "Can't make push!\n";
    exit();
}
