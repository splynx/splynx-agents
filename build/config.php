<?php

$ionCubeEncode = true;
$ionCubeAdditionalParams = '--allow-reflection-all';

$packageDescription = 'Splynx Agents add-on';

$addonBasePackage = 'splynx-addon-base-2-enc';

$controlConfig = [
    'Depends' => '{{addonBasePackage}} (>= {{addonBaseVersion}}), php8.3-phpdbg',
];

$currentReleaseVersion = '5.1';
