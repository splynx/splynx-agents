Splynx Agents Add-on
======================

Splynx Agents Add-on based on [Yii2](http://www.yiiframework.com).

INSTALLATION
------------

Install add-on base:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-addon-base-2.git
cd splynx-addon-base-2
composer install
~~~

Install Splynx Agents Add-on:
~~~
cd /var/www/splynx/addons/
git clone git@bitbucket.org:splynx/splynx-agents.git
cd splynx-agents
composer install
./yii install
~~~

Create symlink:
~~~
ln -s /var/www/splynx/addons/splynx-agents/web/ /var/www/splynx/web/agents
~~~

Create Nginx config file:
~~~
sudo nano /etc/nginx/sites-available/splynx-agents.addons
~~~

with following content:
~~~
location /agents
{
        try_files $uri $uri/ /agents/index.php?$args;
}
~~~

Restart Nginx:
~~~
sudo service nginx restart
~~~

After install add cron job:
~~~
10 23 * * * splynx  /var/www/splynx/addons/splynx-agents/yii cron/define-cancelled-services  >> /var/www/splynx/addons/splynx-agents/runtime/logs/cron.log 2>&1
~~~

Cron will process cancelled services and save cancellation date to customer ADF


Tests:
~~~
./vendor/bin/codecept build
./vendor/bin/codecept run unit
~~~
