<?php

namespace app\base;

use app\helpers\TransactionCategoriesHelper;
use app\models\report\ReportService;
use splynx\helpers\ConfigHelper;
use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use yii\helpers\ArrayHelper;

/**
 * Class Bootstrap
 * @package app\base
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @param Application $app
     */
    public function bootstrap($app)
    {
        $container = Yii::$container;
        $categories = $this->getCategories();
        $container->setSingleton(
            ReportService::class,
            [],
            [array_values($categories), []]
        );
    }

    /**
     * @return array
     */
    protected function getCategories(): array
    {
        $default = [TransactionCategoriesHelper::DISCOUNT, TransactionCategoriesHelper::REFUND];
        $config = ConfigHelper::get('categories');

        if (empty($config)) {
            return [];
        }

        return ArrayHelper::merge($default, $config);
    }
}
