<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Class AppAsset
 * @package app\assets
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
    ];
    public $js = [
        'js/baseDataTableOptions.js',
        'js/datatable-natural-sort.js',
        'js/functions.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
        'yii\bootstrap\BootstrapPluginAsset',
        'splynx\assets\HelperAsset',
        'splynx\assets\Select2Asset',
        'splynx\assets\DataRangePickerAsset',
    ];
    public $jsOptions = ['position' => View::POS_HEAD];
}
