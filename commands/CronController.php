<?php

namespace app\commands;

use app\components\NewCustomerCommission;
use app\models\api\Customer;
use app\models\cron\CronDataRepository;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

class CronController extends Controller
{
    public function actionDefineCancelledServices()
    {
        $customers = (new CronDataRepository())->findCustomersWithCancelledServices();

        foreach ($customers as $customerArray) {
            $customer = (new Customer())->findById($customerArray['id']);

            if ($customer) {
                $customer->additional_attributes[NewCustomerCommission::ADF_CANCELLATION_DATE] = date('Y-m-d');
                if (!$customer->save()) {
                    Yii::error('Error while saving customer ADF: ' . print_r($customer->getErrors(), true));
                }
            }
        }

        return ExitCode::OK;
    }
}
