<?php

namespace app\commands;

use app\components\NewCustomerCommission;
use app\models\api\Vendor;
use splynx\base\BaseInstallController;
use splynx\models\console_api\config\integration\ConsoleModuleConfig;
use yii\base\Exception;

/**
 * Class InstallController
 * @package app\commands
 *
 * @property-read array $entryPoints
 * @property-read string $moduleName
 * @property-read string $addOnTitle
 * @property-read array[] $additionalFields
 * @property-read string $simpleModuleName
 * @property-read array[] $newModulesConfig
 * @property-read array $apiPermissions
 */
class InstallController extends BaseInstallController
{
    /**
     * @var int
     */
    public $module_status = self::MODULE_STATUS_ENABLED;

    /**
     * @var string
     */
    public static $minimumSplynxVersion = '4.3';

    /**
     * @return string
     */
    public function getAddOnTitle(): string
    {
        return 'Splynx Agents';
    }

    /**
     * @return string
     */
    public function getModuleName(): string
    {
        return 'splynx_addon_' . Vendor::ENTITIES;
    }

    /**
     * @return array
     */
    public function getApiPermissions(): array
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
                'actions' => ['index', 'view', 'update'],
            ],
            [
                'controller' => 'api\admin\finance\Invoices',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\finance\Transactions',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\customers\CustomerInternetServices',
                'actions' => ['index', 'view'],
            ],
            [
                'controller' => 'api\admin\config\Module~' . $this->getSimpleModuleName(),
                'actions' => ['index', 'add', 'update', 'view'],
            ],
        ];
    }

    /**
     * @return string
     */
    protected function getSimpleModuleName(): string
    {
        return Vendor::MODULE_NAME;
    }

    /**
     * @return array
     */
    public function getAdditionalFields(): array
    {
        $entryPoints = [
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_name',
                'title' => 'Name',
                'type' => 'string',
                'required' => true,
                'is_add' => true,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_pay_period',
                'title' => 'Paid periods (in months)',
                'type' => 'integer',
                'required' => true,
                'default_value' => 0,
                'is_add' => true,
                'min_length' => 0,
                'max_length' => 100,
                'default' => 0,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_percent',
                'title' => 'Commission percent',
                'type' => 'decimal',
                'required' => true,
                'default_value' => 0,
                'is_add' => true,
                'min_length' => 0,
                'max_length' => 100,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_fixed_amount',
                'title' => 'Commission fixed amount',
                'type' => 'decimal',
                'required' => true,
                'default_value' => 0,
                'is_add' => true,
                'min_length' => 0,
                'max_length' => 100000,
            ],
            [
                'main_module' => $this->getSimpleModuleName(),
                'name' => $this->getSimpleModuleName() . '_email',
                'title' => 'Email',
                'type' => 'string',
                'is_add' => true,
            ],
            [
                'main_module' => 'customers',
                'name' => $this->getModuleName() . '_' . Vendor::ENTITY,
                'title' => ucfirst(Vendor::ENTITY),
                'type' => 'relation',
                'relation_module' => $this->getSimpleModuleName(),
                'required' => false,
            ],
            [
                'main_module' => 'customers',
                'name' => NewCustomerCommission::ADF_AMOUNT,
                'title' => 'First internet service amount',
                'type' => 'decimal',
                'required' => false,
                'default_value' => 0,
                'is_add' => true,
                'hidden' => true,
            ],
            [
                'main_module' => 'customers',
                'name' => NewCustomerCommission::ADF_CANCELLATION_DATE,
                'title' => 'First internet service cancellation date',
                'type' => 'date',
                'required' => false,
                'is_add' => true,
                'hidden' => true,
            ],
        ];

        $commissionsEntryPoint = [
            'main_module' => $this->getSimpleModuleName(),
            'name' => $this->getSimpleModuleName() . '_start_percent',
            'title' => 'Commission for new customer (% from internet service price)',
            'type' => 'decimal',
            'required' => false,
            'default_value' => 0,
            'is_add' => true,
            'min_length' => 0,
            'max_length' => 100,
            'show_in_list' => false,
        ];

        if ($this->isInstallProcess()) {
            $commissionsEntryPoint['hidden'] = true;
        }

        $entryPoints[] = $commissionsEntryPoint;

        return $entryPoints;
    }

    /**
     * @return array
     */
    public function getEntryPoints(): array
    {
        return [
            [
                'name' => Vendor::ENTITIES . '_main',
                'title' => Vendor::getEntitiesName(),
                'root' => 'controllers\admin\administration\ReportsController',
                'place' => 'admin',
                'url' => '/' . Vendor::ENTITIES . '/',
                'icon' => 'fa-calculator',
                'background' => '#bf360c',
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function actionIndex()
    {
        $modules = $this->getNewModulesConfig();

        $install = false;
        foreach ($modules as $item) {
            $module = (new ConsoleModuleConfig())->findOne([
                'module' => $item['module'],
                'type' => $item['type'],
            ]);
            if (empty($module)) {
                $install = true;
                unset($item['title_attribute_prefix']);
                $module = new ConsoleModuleConfig($item);

                if (!$module->save()) {
                    exit('Can`t create module ' . $item['module'] . "\n");
                }
            }
        }

        parent::actionIndex();

        if ($install) {
            foreach ($modules as $item) {
                $module = (new ConsoleModuleConfig())->findOne([
                    'module' => $item['module'],
                    'type' => $item['type'],
                ]);
                if (empty($module)) {
                    exit('Can`t find module ' . $item['module'] . "\n");
                }
                $module->title_attribute_prefix = $item['title_attribute_prefix'];

                if (!$module->update()) {
                    exit('Can`t update module ' . $item['module'] . "\n");
                }
            }
        }
    }

    /**
     * Get configurations for the installing modules
     * @return array
     */
    protected function getNewModulesConfig(): array
    {
        return [
            [
                'module' => $this->getSimpleModuleName(),
                'title' => Vendor::getEntitiesName(),
                'root' => 'controllers\admin\FinanceController',
                'type' => ConsoleModuleConfig::TYPE_SIMPLE,
                'icon' => 'fa-user-secret',
                'title_attribute_prefix' => $this->getSimpleModuleName() . '_name',
            ],
        ];
    }
}
