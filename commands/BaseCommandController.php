<?php

namespace app\commands;

use app\helpers\DateHelper;
use Yii;
use yii\base\InvalidRouteException;
use yii\console\Controller;
use yii\console\Exception;

/**
 * Class ToolsController
 * @package app\commands
 */
class BaseCommandController extends Controller
{
    /**
     * Show help for command actions
     * @throws InvalidRouteException
     * @throws Exception
     */
    public function actionIndex()
    {
        $route = $this->getUniqueId();
        Yii::$app->runAction('help', [$route]);
    }

    /**
     * @inheritdoc
     */
    public function stdout($string)
    {
        $args = func_get_args();
        $args[0] = DateHelper::getDateWithMilliseconds() . ' ' . $string;

        return parent::stdout(...$args);
    }

    /**
     * @inheritdoc
     */
    public function stderr($string)
    {
        $args = func_get_args();
        $args[0] = DateHelper::getDateWithMilliseconds() . ' ' . $string;
        Yii::error($string);

        return parent::stderr(...$args);
    }
}
