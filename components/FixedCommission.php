<?php

namespace app\components;

use app\models\api\Vendor;
use app\models\report\ReportDataRow;
use Yii;
use DateTime;
use DateTimeZone;
use DateInterval;
use DatePeriod;

class FixedCommission
{
    /**
     * @var DateTimeZone $_timezone Timezone object for correct dates diffs behavior
     */
    private $_timezone;
    /**
     * @var Formatter
     */
    private $_formatter;

    private $_endDates = [];
    private $_periods = [];
    private $_amounts = [];
    private $_monthMap = [];

    private $_reportStartDate;

    public $comissions = [];

    /**
     * @param string $reportStartDate
     * @param array $settings
     */
    public function __construct(string $reportStartDate, array $settings)
    {
        $this->_reportStartDate = $reportStartDate;

        $this->_timezone = $settings['timezone'] ?? new DateTimeZone('UTC');
        $this->_formatter = $settings['formatter'] ?? Yii::$app->getFormatter();
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendorProperties(Vendor $vendor)
    {
        $this->setPeriodByVendor($vendor);
        $this->setAmountByVendor($vendor);
    }

    /**
     * @param Vendor $vendor
     */
    public function setPeriodByVendor(Vendor $vendor)
    {
        $this->_periods[$vendor->id] = (int)$vendor->additional_attributes[Vendor::PAY_PERIOD];
    }

    /**
     * @param Vendor $vendor
     */
    public function setAmountByVendor(Vendor $vendor)
    {
        $this->_amounts[$vendor->id] = (float)$vendor->additional_attributes[Vendor::FIXED_AMOUNT];
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @param string $customerStartDate
     * @param int $vendorPeriod
     */
    public function setEndDateByCustomer(int $vendorId, int $customerId, string $customerStartDate, int $vendorPeriod)
    {
        if (!isset($this->_endDates[$vendorId][$customerId])) {
            $this->_endDates[$vendorId][$customerId] = $this->getVendorPayPeriodMaxDate($customerStartDate, $vendorPeriod);
        }
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @param string $endDate
     */
    public function setEndDate(int $vendorId, int $customerId, string $endDate)
    {
        $this->_endDates[$vendorId][$customerId] = $endDate;
    }

    public function setMonthMap(int $vendorId, int $customerId, array $monthMap)
    {
        $this->_monthMap[$vendorId][$customerId] = $monthMap;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return string
     */
    public function getEndDate(int $vendorId, int $customerId)
    {
        return ($this->_endDates[$vendorId][$customerId] ?? null);
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array
     */
    public function getMonthMap(int $vendorId = null, int $customerId = null): array
    {
        if (!empty($customerId) && isset($this->_monthMap[$vendorId][$customerId])) {
            return $this->_monthMap[$vendorId][$customerId];
        }
        if (!empty($vendorId) && isset($this->_monthMap[$vendorId])) {
            return $this->_monthMap[$vendorId];
        }
        return $this->_monthMap;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return boolean
     */
    public function isMonthMapPopulatedForCustomer(int $vendorId, int $customerId): bool
    {
        return isset($this->_monthMap[$vendorId][$customerId]);
    }

    /**
     * define if we can pay fixed commission to $vendor for $customer after $reportStartDate;
     * it indicates only if we can pay but not if we have to pay
     * @param int $vendorId
     * @param int $customerId
     * @return boolean
     */
    public function canPayToVendorForCustomer(int $vendorId, int $customerId): bool
    {
        return (
            !empty($this->_amounts[$vendorId]) &&
            (
                empty($this->_periods[$vendorId]) ||
                (
                    isset($this->_endDates[$vendorId][$customerId]) &&
                    $this->_endDates[$vendorId][$customerId] > $this->_reportStartDate
                )
            )
        );
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @param string $fromDate
     * @param array $maxCoveredDates
     * @return array
     */
    public function populateCoveredMonthsMapByCustomer(int $vendorId, int $customerId, string $fromDate, array $maxCoveredDates): array
    {
        if ($this->canPayToVendorForCustomer($vendorId, $customerId)) {
            $maxCoveredDate = ($maxCoveredDates[$customerId] ?? null);
            $vendorEndDate = $this->_endDates[$vendorId][$customerId];

            if ($maxCoveredDate) {
                if (!empty($vendorEndDate) && $vendorEndDate < $maxCoveredDate) {
                    $maxCoveredDate = $vendorEndDate;
                }

                $this->_monthMap[$vendorId][$customerId] = $this->populateMap([], $fromDate, $maxCoveredDate);
            }
        }

        return $this->_monthMap;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @param ReportDataRow $transaction
     * @return array
     */
    public function populateMonthsMapWithTransaction(int $vendorId, int $customerId, ReportDataRow $transaction): array
    {
        if (!isset($this->_monthMap[$vendorId][$customerId])) {
            $this->_monthMap[$vendorId][$customerId] = [];
        }
        $transactionStartDateString = $transaction->period_from !== '0000-00-00' ? $transaction->period_from : $transaction->date;
        $transactionEndDateString = $transaction->period_from !== '0000-00-00' ? $transaction->period_to : $transaction->date;
        $this->_monthMap[$vendorId][$customerId] = $this->populateMap($this->_monthMap[$vendorId][$customerId], $transactionStartDateString, $transactionEndDateString, true);

        return $this->_monthMap;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array|boolean
     */
    public function getReportRowForCustomerReport(int $vendorId, int $customerId)
    {
        $result = $this->calculateFixedAmountByCustomer($vendorId, $customerId);

        if (is_array($result)) {
            return [
                'billing_transactions_id' => 'fixed-commission',
                'category' => '',
                'description' => sprintf(Yii::t('app', 'Fixed commission for %d months (%s)'), $result['monthsForComission'], $result['description']),
                'date' => '',
                'period_from' => '',
                'period_to' => '',
                'invoice_id' => '',
                'total' => '0',
                'commission' => $this->_formatter->asDecimal($result['amount'], 2),
            ];
        }
        return false;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array|boolean
     */
    public function getReportRowForVendorReport(int $vendorId, int $customerId, float $transactionsCommission)
    {
        $result = $this->calculateFixedAmountByCustomer($vendorId, $customerId);

        if (is_array($result) && !empty($result['amount'])) {
            return $this->_formatter->asDecimal($transactionsCommission + $result['amount'], 2)
                . ' ' . sprintf(Yii::t('app', '(including %s as fixed commission for %s)'), $this->_formatter->asDecimal($result['amount'], 2), $result['description']);
        }
        return false;
    }

    /**
     * @param int $vendorId
     * @return array|bool
     */
    public function getReportRowForConsolidatedReport(int $vendorId)
    {
        $fixedAmountByVendor = 0;
        foreach ($this->_monthMap[$vendorId] as $customerId => $customerMonths) {
            $fixedAmountByCustomer = $this->calculateFixedAmountByCustomer($vendorId, $customerId);
            if (is_array($fixedAmountByCustomer) && !empty($fixedAmountByCustomer['amount'])) {
                $fixedAmountByVendor += $fixedAmountByCustomer['amount'];
            }
        }

        return $fixedAmountByVendor ? [
            'amount' => $fixedAmountByVendor,
            'description' => '<br>' . sprintf(Yii::t('app', '(including %s as fixed commission)'), $this->_formatter->asDecimal($fixedAmountByVendor, 2)),
        ] : false;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array
     */
    public function getCommissionByCustomerForVendorReport(int $vendorId, int $customerId)
    {
        $result = $this->calculateFixedAmountByCustomer($vendorId, $customerId);

        if (is_array($result) && !empty($result['amount'])) {
            return [
                'amount' => $result['amount'],
                'description' => sprintf(Yii::t('app', '(including %s as fixed commission for %s)'), $this->_formatter->asDecimal($result['amount'], 2), $result['description']),
            ];
        }
        return [];
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return boolean|array
     */
    protected function calculateFixedAmountByCustomer(int $vendorId, int $customerId)
    {
        $monthMap = $this->_monthMap[$vendorId][$customerId] ?? [];

        if ($this->canPayToVendorForCustomer($vendorId, $customerId) && sizeof($monthMap)) {
            $monthsForComission = 0;
            $monthsForComissionDescription = '';

            ksort($monthMap);

            $countedMonths = [];
            foreach ($monthMap as $monthName => $comissionPayed) {
                if (!$comissionPayed && (empty($this->_endDates[$vendorId][$customerId]) || $monthName . '-01' <= $this->_endDates[$vendorId][$customerId])) {
                    $monthsForComission++;
                    $countedMonths[] = $this->_formatter->asDate($monthName . '-01', 'MMM Y');
                }
            }
            if ($monthsForComission) {
                $monthsForComissionDescription = $countedMonths[0] . ($monthsForComission > 1 ? ' - ' . $countedMonths[sizeof($countedMonths) - 1] : '');
            }

            if ($monthsForComission) {
                return [
                    'amount' => $this->_amounts[$vendorId] * $monthsForComission,
                    'monthsForComission' => $monthsForComission,
                    'description' => $monthsForComissionDescription,
                ];
            }
        }
        return false;
    }

    /**
     * return last date of month $startDate+$vendorComissionPeriod or empty when $vendorComissionPeriod is infinitive
     * @param string $startDate date of first customer transaction
     * @param int $vendorComissionPeriod
     * @return string
     */
    protected function getVendorPayPeriodMaxDate(string $startDate, int $vendorComissionPeriod): string
    {
        if (empty($vendorComissionPeriod)) {
            return '';
        }

        $vendorEndDateObj = $this->getFirstDayOfMonthDate($startDate);
        $vendorEndDateObj->modify('+ ' . ($vendorComissionPeriod - 1) . ' month');
        $vendorEndDateObj->modify('last day of this month');

        return $vendorEndDateObj->format('Y-m-d');
    }

    /**
     * @param array $monthMap
     * @param string $fromDate
     * @param string $maxCoveredDate
     * @param bool $isNew
     * @return array
     */
    protected function populateMap(array $monthMap, string $fromDate, string $maxCoveredDate, bool $isNew = false): array
    {
        $months = $this->getMonthsBetweenTwoDates($fromDate, $maxCoveredDate);

        foreach ($months as $month) {
            if (!array_key_exists($month, $monthMap)) {
                $monthMap[$month] = ($isNew ? 0 : 1);
            }
        }

        return $monthMap;
    }

    /**
     * @param string $startDateString
     * @param string $endDateString
     * @return array
     */
    protected function getMonthsBetweenTwoDates(string $startDateString, string $endDateString): array
    {
        $startDate = (new DateTime($startDateString, $this->_timezone))->modify('first day of this month');
        $endDate = (new DateTime($endDateString, $this->_timezone))->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($startDate, $interval, $endDate);

        $months = [];
        foreach ($period as $dt) {
            $months[] = $dt->format('Y-m');
        }

        return $months;
    }

    /**
     * @param string $date 'Y-m-d'
     * @return DateTime
     */
    protected function getFirstDayOfMonthDate(string $date): DateTime
    {
        $result = new DateTime($date, $this->_timezone);
        $result->modify('first day of this month');

        return $result;
    }
}
