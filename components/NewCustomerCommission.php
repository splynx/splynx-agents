<?php

namespace app\components;

use app\models\api\Vendor;
use app\models\report\ReportDataRepository;
use app\helpers\ReportDataHelper;
use Yii;
use DateTimeZone;

/**
 * commission for a new customer should be showed for the month
 * when this customer was converted from lead
 * negative commission for cancelling of internet services
 */
class NewCustomerCommission
{
    public const ADF_AMOUNT = 'report_first_service_amount';
    public const ADF_CANCELLATION_DATE = 'report_first_service_cancel_date';

    /*
     * negative commission is applied
     * if service was cancelled during this period (months) after convertation
     */
    public const CANCELLATION_PERIOD = 6;
    /**
     * @var DateTimeZone $_timezone Timezone object for correct dates diffs behavior
     */
    private $_timezone;
    /**
     * @var Formatter
     */
    private $_formatter;
    private $_reportStartDate;
    private $_reportEndDate;

    /*
     * commissions for converted customers
     */
    public $commissions = [];
    /*
     * negative commissions if customers' services were cancelled in 6 months
     */
    public $negativeCommissions = [];

    /**
     * @param string $reportStartDate
     * @param array $settings
     */
    public function __construct(string $reportStartDate, string $reportEndDate, array $settings)
    {
        $this->_reportStartDate = $reportStartDate;
        $this->_reportEndDate = $reportEndDate;

        $this->_timezone = $settings['timezone'] ?? new DateTimeZone('UTC');
        $this->_formatter = $settings['formatter'] ?? Yii::$app->getFormatter();
    }

    /**
     * @param Vendor $vendor
     */
    public function setVendorProperties(Vendor $vendor)
    {
        $this->setPeriodByVendor($vendor);
        $this->setCommissionPercentByVendor($vendor);
    }

    /**
     * @param Vendor $vendor
     */
    public function setPeriodByVendor(Vendor $vendor)
    {
        $this->_periods[$vendor->id] = (int)$vendor->additional_attributes[Vendor::PAY_PERIOD];
    }

    /**
     * @param Vendor $vendor
     */
    public function setCommissionPercentByVendor(Vendor $vendor)
    {
        $this->_percents[$vendor->id] = (float)$vendor->additional_attributes[Vendor::NEW_CUSTOMER_PERCENT];
    }

    /**
     * get customers who are new for current report period
     * @param int $vendorId
     * @param int|null $customerId
     */
    public function populateNewCustomers(int $vendorId, int $customerId = null)
    {
        $newCustomers = (new ReportDataRepository())->findNewCustomers($this->_reportStartDate, $this->_reportEndDate, [$vendorId], $customerId);

        foreach ($newCustomers as $customer) {
            $this->commissions[$vendorId][$customer['id']] = $this->calculateCommissionByCustomer($vendorId, $customer);
        }
    }

    /**
     * get customers whose services were cancelled
     * @param int $vendorId
     * @param int|null $customerId
     */
    public function populateCustomersWithCancelledService(int $vendorId, int $customerId = null)
    {
        $customers = (new ReportDataRepository())->findCustomersWithCancelledService($this->_reportStartDate, $this->_reportEndDate, [$vendorId], $customerId);

        foreach ($customers as $customer) {
            $this->negativeCommissions[$vendorId][$customer['id']] = $this->calculateNegativeCommissionByCustomer($vendorId, $customer);
        }
    }

    /**
     * @param int $vendorId
     * @return bool
     */
    public function isNewCustomersPopulated(int $vendorId)
    {
        return isset($this->commissions[$vendorId]) || isset($this->negativeCommissions[$vendorId]);
    }

    /**
     * @param int $vendorId
     * @param array $customer
     * @return array
     */
    protected function calculateCommissionByCustomer(int $vendorId, array $customer)
    {
        if (empty($this->_percents[$vendorId])) {
            return [];
        }
        $commission = ($customer['quantity'] * $customer['unit_price'] * $this->_percents[$vendorId]) / 100;

        return [
            'name' => $customer['name'],
            'amount' => $commission,
            'description' => sprintf(Yii::t('app', '(including %s as commission for new customer)'), $this->_formatter->asDecimal($commission, 2)),
        ];
    }

    /**
     * @param int $vendorId
     * @param array $customer
     * @return array
     */
    protected function calculateNegativeCommissionByCustomer(int $vendorId, array $customer)
    {
        if (empty($this->_percents[$vendorId])) {
            return [];
        }
        $commission = 0 - ($customer['service_amount'] * $this->_percents[$vendorId]) / 100;

        return [
            'name' => $customer['name'],
            'negative_amount' => $commission,
            'negative_description' => sprintf(Yii::t('app', '(%s commission for cancelled services)'), $this->_formatter->asDecimal($commission, 2)),
        ];
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array
     */
    public function getCommissionByCustomer(int $vendorId, int $customerId)
    {
        return isset($this->commissions[$vendorId][$customerId]) ? $this->commissions[$vendorId][$customerId] : [];
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array
     */
    public function getNegativeCommissionByCustomer(int $vendorId, int $customerId)
    {
        return isset($this->negativeCommissions[$vendorId][$customerId]) ? $this->negativeCommissions[$vendorId][$customerId] : [];
    }

    /**
     * @param int $vendorId
     * @return bool|array
     */
    public function getReportRowForConsolidatedReport(int $vendorId)
    {
        $newCustomersCommission = 0;
        if (isset($this->commissions[$vendorId])) {
            foreach ($this->commissions[$vendorId] as $customerFields) {
                $newCustomersCommission += $customerFields['amount'];
            }
        }
        $negativeCommission = 0;
        if (isset($this->negativeCommissions[$vendorId])) {
            foreach ($this->negativeCommissions[$vendorId] as $customerFields) {
                $negativeCommission += $customerFields['negative_amount'];
            }
        }
        $totalCommission = $newCustomersCommission + $negativeCommission;

        return $newCustomersCommission || $negativeCommission ? [
            'amount' => $totalCommission,
            'description' => ($newCustomersCommission
                    ? '<br>' . sprintf(Yii::t('app', '(including %s as new customers commission)'), $this->_formatter->asDecimal($newCustomersCommission, 2))
                    : '')
                . ($negativeCommission
                    ? '<br>' . sprintf(Yii::t('app', '(%s commission for cancelled services)'), $this->_formatter->asDecimal($negativeCommission, 2))
                    : ''),
        ] : false;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array|boolean
     */
    public function getReportRowForCustomerReport(int $vendorId, int $customerId)
    {
        if (!empty($this->commissions[$vendorId][$customerId]['amount'])) {
            return [
                'billing_transactions_id' => 'new-customer',
                'category' => '',
                'description' => sprintf(Yii::t('app', 'Commission for new customer')),
                'date' => '',
                'period_from' => '',
                'period_to' => '',
                'invoice_id' => '',
                'total' => 0,
                'commission' => $this->_formatter->asDecimal($this->commissions[$vendorId][$customerId]['amount'], 2),
            ];
        }
        return false;
    }

    /**
     * @param int $vendorId
     * @param int $customerId
     * @return array|boolean
     */
    public function getReportNegativeRowForCustomerReport(int $vendorId, int $customerId)
    {
        if (!empty($this->negativeCommissions[$vendorId][$customerId]['negative_amount'])) {
            return [
                'billing_transactions_id' => 'cancelled-service',
                'category' => '',
                'description' => sprintf(Yii::t('app', 'Commission for cancelled service')),
                'date' => '',
                'period_from' => '',
                'period_to' => '',
                'invoice_id' => '',
                'total' => 0,
                'commission' => $this->_formatter->asDecimal($this->negativeCommissions[$vendorId][$customerId]['negative_amount'], 2),
            ];
        }
        return false;
    }

    /**
     * @param int $vendorId
     * @param array $result
     * @return array
     */
    public function mergeNewCustomersToVendorReport($vendorId, $result)
    {
        $merge = function ($commissionsSet) use (&$result, $vendorId) {
            if (!isset($commissionsSet[$vendorId])) {
                return;
            }
            foreach ($commissionsSet[$vendorId] as $customerId => $customer) {
                if (!isset($result[$customerId])) {
                    $result[$customerId] = [
                        'customer_id' => $customerId,
                        'customer_name' => ReportDataHelper::generateCustomerLink($customer['name'], $this->_reportStartDate, $this->_reportEndDate, $customerId),
                        'transactions_count' => 0,
                        'total_sum' => 0,
                        'commission_total' => 0,
                    ];
                }
            }
        };

        $merge($this->commissions);
        $merge($this->negativeCommissions);

        return $result;
    }
}
