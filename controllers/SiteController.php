<?php

namespace app\controllers;

use app\models\api\Customer;
use app\models\api\Vendor;
use app\models\report\ReportService;
use DateTime;
use Exception;
use splynx\base\ApiResponseException;
use splynx\helpers\DateHelper;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;
use yii\base\UserException;
use yii\di\NotInstantiableException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\ErrorAction;

/**
 * Class SiteController
 * @package app\controllers
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions(): array
    {
        return [
            'error' => [
                'class' => ErrorAction::class,
            ],
        ];
    }

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Renders consolidated report page. Loads datatable data for report.
     * @return array|string
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws \yii\db\Exception
     */
    public function actionIndex()
    {
        $this->getView()->title = Yii::t('app', 'Splynx agents consolidated report');
        $request = Yii::$app->request;
        ['from' => $from, 'to' => $to, 'startDate' => $startDate, 'endDate' => $endDate] = $this->getPeriodDates($request->get('period'));
        if (!$period = $request->get('period')) {
            $period = $startDate . ' - ' . $endDate;
        }
        $params = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'link' => Yii::$app->request->getBaseUrl() . '?list=yes&period=' . $period,
            'entity' => Vendor::ENTITY,
        ];
        // Dt response
        if ($request->get('list') === 'yes') {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $service = $this->getReportService();

            return ['data' => array_values($service->generateVendorsReport($from, $to))];
        }

        $selectedFields = [
            'name' => Yii::t('app', 'Agent'),
            'active_customers_count' => [
                'title' => Yii::t('app', 'Commissioned transactions'),
                'type' => 'num',
            ],
            'unpaid_invoices_total' => [
                'title' => Yii::t('app', 'Unpaid total'),
                'type' => 'num',
            ],
            'total_sum' => [
                'title' => Yii::t('app', 'Total'),
                'type' => 'num',
            ],
            'commission_total' => [
                'title' => Yii::t('app', 'Commission'),
                'type' => 'natural',
            ],
        ];

        return $this->render('vendors', [
            'tableColumnNames' => $this->generateDTColumns($selectedFields),
            'params' => $params,
        ]);
    }

    /**
     * Renders consolidated report page. Loads datatable data for report.
     * @return array|string
     * @throws ApiResponseException
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws \yii\db\Exception
     */
    public function actionVendorReport()
    {
        $this->getView()->title = Yii::t('app', 'Splynx agent report');
        $request = Yii::$app->request;
        ['from' => $from, 'to' => $to, 'startDate' => $startDate, 'endDate' => $endDate] = $this->getPeriodDates($request->get('period'));
        if (!$period = $request->get('period')) {
            $period = $startDate . ' - ' . $endDate;
        }
        $params = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'link' => Yii::$app->request->getBaseUrl() . '/vendor-report?list=yes',
            'entity' => Vendor::ENTITY,
        ];
        // Url filter
        if ($vendorId = $request->get('vendor')) {
            $params['link'] = Yii::$app->request->getBaseUrl() . '/vendor-report?list=yes&period=' . $period . '&vendor=' . $vendorId;
        }
        // Dt response
        if ($request->get('list') === 'yes') {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!$vendorId) {
                return ['data' => []];
            }

            $service = $this->getReportService();

            return ['data' => array_values($service->generateVendorReport($from, $to, $vendorId))];
        }

        $selectedFields = [
            'customer_name' => Yii::t('app', 'Customer'),
            'transactions_count' => [
                'title' => Yii::t('app', 'Commissioned transactions'),
                'type' => 'num',
            ],
            'unpaid_invoices_ids' => [
                'title' => Yii::t('app', 'Unpaid invoices IDs'),
                'type' => 'num',
            ],
            'unpaid_invoices_total' => [
                'title' => Yii::t('app', 'Unpaid total'),
                'type' => 'num',
            ],
            'total_sum' => [
                'title' => Yii::t('app', 'Total'),
                'type' => 'num',
            ],
            'commission_total' => [
                'title' => Yii::t('app', 'Commission'),
                'type' => 'natural',
            ],
        ];

        $data = [];
        if ($vendorId && ($vendor = Vendor::findByIdStatic((int)$vendorId))) {
            $data[] =
                [
                    'id' => $vendor->id,
                    'text' => $vendor->additional_attributes[Vendor::NAME],
                    'selected' => true,
                ];
        }

        $select2Params = [
            'id' => 'vendor-filter-vendor',
            'name' => Vendor::ENTITY,
            'pluginOptions' => [
                'placeholder' => 'Select ' . Vendor::ENTITY . ' ...',
                'minimumInputLength' => 3,
                'data' => $data,
                'ajax' => [
                    'url' => Yii::$app->request->getBaseUrl() . '/search-vendors',
                    'dataType' => 'json',
                    'delay' => 250,
                    'cache' => true,
                ],
            ],
        ];

        return $this->render('vendor', [
            'tableColumnNames' => $this->generateDTColumns($selectedFields),
            'params' => $params,
            'select2Params' => $select2Params,
        ]);
    }

    /**
     * Renders consolidated customer report page. Loads datatable data for report.
     * @return array|string
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     * @throws ApiResponseException
     * @throws \yii\db\Exception
     */
    public function actionCustomerReport()
    {
        $this->getView()->title = Yii::t('app', 'Splynx customer report');
        $request = Yii::$app->request;
        ['from' => $from, 'to' => $to, 'startDate' => $startDate, 'endDate' => $endDate] = $this->getPeriodDates($request->get('period'));
        if (!$period = $request->get('period')) {
            $period = $startDate . ' - ' . $endDate;
        }
        $params = [
            'startDate' => $startDate,
            'endDate' => $endDate,
            'link' => Yii::$app->request->getBaseUrl() . '/customer-report?list=yes',
            'entity' => Vendor::ENTITY,
        ];
        // Url filter
        if ($customerId = $request->get('customer')) {
            $params['customer'] = $customerId;
            $params['link'] = Yii::$app->request->getBaseUrl() . '/customer-report?list=yes&period=' . $period . '&customer=' . $customerId;
        }
        // Dt response
        if ($request->get('list') === 'yes') {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if (!$customerId) {
                return ['data' => []];
            }
            $service = $this->getReportService();

            return ['data' => array_values($service->generateCustomerReport($from, $to, $customerId))];
        }

        $selectedFields = [
            'billing_transactions_id' => [
                'title' => Yii::t('app', 'Transaction ID'),
                'type' => 'num',
            ],
            'category' => [
                'title' => Yii::t('app', 'Category'),
                'type' => 'num',
            ],
            'description' => Yii::t('app', 'Description'),
            'date' => Yii::t('app', 'Date'),
            'period_from' => Yii::t('app', 'From'),
            'period_to' => Yii::t('app', 'To'),
            'invoice_id' => [
                'title' => Yii::t('app', 'Invoice ID'),
                'type' => 'num',
            ],
            'total' => [
                'title' => Yii::t('app', 'Total'),
                'type' => 'num',
            ],
            'commission' => [
                'title' => Yii::t('app', 'Commission'),
                'type' => 'num',
            ],
        ];

        $data = [];
        if ($customerId && ($customer = Customer::findByIdStatic((int)$customerId))) {
            $data[] = [
                'id' => $customer->id,
                'text' => $customer->name,
                'selected' => true,
            ];
        }

        $select2Params = [
            'id' => 'customer-filter-customer',
            'name' => 'customer',
            'pluginOptions' => [
                'placeholder' => Yii::t('app', 'Select customer...'),
                'data' => $data,
                'minimumInputLength' => 3,
                'ajax' => [
                    'url' => Yii::$app->request->getBaseUrl() . '/search-customers',
                    'dataType' => 'json',
                    'delay' => 250,
                ],
            ],
        ];

        return $this->render('customer', [
            'tableColumnNames' => $this->generateDTColumns($selectedFields),
            'params' => $params,
            'select2Params' => $select2Params,
        ]);
    }

    /**
     * Search customers by name
     * @return array
     */
    public function actionSearchCustomers(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $query = Yii::$app->request->get('q');
        if (empty($query) || (!$results = Customer::findByName($query))) {
            return ['results' => []];
        }

        $data = [];
        foreach ($results as $result) {
            $data[] = [
                'id' => $result->id,
                'text' => $result->name,
            ];
        }
        $out['results'] = $data;

        return $out;
    }

    /**
     * Search vendors by name
     * @return array
     */
    public function actionSearchVendors(): array
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $query = Yii::$app->request->get('q');
        if (empty($query) || (!$results = Vendor::findByName($query))) {
            return ['results' => []];
        }

        $data = [];
        foreach ($results as $result) {
            $data[] = [
                'id' => $result->id,
                'text' => $result->additional_attributes[Vendor::NAME],
            ];
        }
        $out['results'] = $data;

        return $out;
    }

    /**
     * @return ReportService|object
     * @throws InvalidConfigException
     * @throws NotInstantiableException
     */
    protected function getReportService()
    {
        return Yii::$container->get(ReportService::class);
    }

    /**
     * Returns array with sets of dates formatted for backend and daterangepicker
     * [
     *      from => Y-m-d for backend
     *      to => Y-m-d for backend
     *      startDate => formatted for daterangepicker
     *      endDate => formatted for daterangepicker
     * ]
     * @param string|null $period
     * @return array
     * @throws InvalidArgumentException
     */
    protected function getPeriodDates(string $period = null): array
    {
        $result = [];
        $format = DateHelper::getSplynxDateFormat();
        // Date range picker period
        if ($period) {
            $periodDates = explode(' - ', $period);
            if (count($periodDates) !== 2) {
                throw new InvalidArgumentException('Invalid period: ' . $period);
            }
            [$fromDateString, $toDateString] = $periodDates;
            $result['startDate'] = $fromDateString;
            $result['endDate'] = $toDateString;
            try {
                $result['from'] = DateTime::createFromFormat($format, $fromDateString)->format('Y-m-d');
                $result['to'] = DateTime::createFromFormat($format, $toDateString)->format('Y-m-d');

                return $result;
            } catch (Exception $e) {
                throw new UserException('Wrong period!');
            }
        }
        // Default period
        $date = new DateTime('-1 month');
        $date->modify('first day of this month');
        $result['startDate'] = $date->format($format);
        $result['from'] = $date->format('Y-m-d');
        $date->modify('last day of this month');
        $result['to'] = $date->format('Y-m-d');
        $result['endDate'] = $date->format($format);

        return $result;
    }

    /**
     * Returns json encoded data table columns
     * @param array $columns Each value is 'title' (string) OR config containing 'title' (array)
     * @return string
     */
    protected function generateDTColumns(array $columns): string
    {
        $result = [];
        foreach ($columns as $name => $value) {
            $result[] = is_array($value) ? array_merge(['data' => $name], $value) : ['data' => $name, 'title' => $value];
        }

        return json_encode($result);
    }
}
