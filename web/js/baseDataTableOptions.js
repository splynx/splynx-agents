$.extend($.fn.DataTable.defaults, {
    oLanguage: {
        oPaginate: {
            "sFirst": "<i class='icon-ic_fluent_arrow_previous_24_regular'></i>",
            "sPrevious": "<i class='icon-ic_fluent_chevron_left_24_regular'></i>",
            "sNext": "<i class='icon-ic_fluent_chevron_right_24_regular'></i>",
            "sLast": "<i class='icon-ic_fluent_arrow_next_24_regular'></i>"
        },
        sEmptyTable: "No data available in table",
        sInfo: "Showing _START_ to _END_ of _TOTAL_ entries",
        sInfoEmpty: "Showing 0 to 0 of 0 entries",
        sInfoFiltered: "(filtered from _MAX_ total entries)",
        sInfoPostFix: "",
        sLengthMenu: "Show _MENU_ entries",
        sLoadingRecords: "Loading...",
        sProcessing: "Processing...",
        sSearch: "Search:",
        sUrl: "",
        sZeroRecords: "No matching records found",
        oAria: {
            "sSortAscending": ": activate to sort column ascending",
            "sSortDescending": ": activate to sort column descending"
        }
    },
    fnInitComplete: function (oSettings, json) {
        $(".dataTables_length select").select2({
            minimumResultsForSearch: -1
        });
    },
    dom: "<'row'<'col-md-12'<'table_title table-title'>>><'row'<'col-md-12'<'dt-actions has-mass-actions'<'dt-show-items'l><'dt-search'f><'dt-buttons-wrap'B>r>>><'row'<'col-md-12 dt-under-header'>>t<'row justify-content-end under-table'<'info' i>p>",
    order: [[0, 'desc']],
    pageLength: 100,
    destroy: true,
});