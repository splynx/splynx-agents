var parameterWithFooter = [
    { extend: 'print', footer: true },
    { extend: 'copyHtml5', footer: true },
    { extend: 'excelHtml5', footer: true },
    { extend: 'csvHtml5', footer: true },
    { extend: 'pdfHtml5', footer: true }
]
Object.defineProperty( dataTableButtons[0], 'buttons', {value : parameterWithFooter});