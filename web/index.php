<?php

use splynx\base\WebApplication;

// Check to enable development mode
if (file_exists(__DIR__ . '/../config/dev.php')) {
    require(__DIR__ . '/../config/dev.php');
}

// Load Splynx Base Add-on vendor
if (defined('YII_ENV') && YII_ENV == 'dev') { /* @phpstan-ignore-line */
    require(__DIR__ . '/../../splynx-addon-base-2/vendor/autoload.php');
    require(__DIR__ . '/../../splynx-addon-base-2/vendor/yiisoft/yii2/Yii.php');
} else {
    require(__DIR__ . '/../../splynx-addon-base-2-enc/vendor/autoload.php');
    require(__DIR__ . '/../../splynx-addon-base-2-enc/vendor/yiisoft/yii2/Yii.php');
}

// Load add-on vendor
require(__DIR__ . '/../vendor/autoload.php');

$baseDir = dirname(__DIR__);
$configPath = $baseDir . '/config/web.php';

(new WebApplication($baseDir, $configPath))->run();
